#pragma once
#include <cstdint>
#include <vector>
#include "device.h"

namespace ovutil
{
/**
 * Abstract base class for motor controllers
 */
class Controller : public Device
{
  public:
    enum typecode : uint8_t;
    
    Controller(canid_t id, typecode t, const std::string &name);
    virtual ~Controller() {}
    /**
     * Get type code and string
     */
    typecode getType() const;
    std::string getTypeString() const;
    /**
     * Save current parameters as XML elements under parent
     */
    void saveAsXML(tinyxml2::XMLElement* parent, tinyxml2::XMLDocument &doc) const;
    /**
     * Conversion utils between type codes and type strings
     */
    static std::string typeToString(typecode t);
    static typecode stringToType(const std::string &str);
    /**
     * Controller type codes
     */
    enum typecode : uint8_t
    {
        PIKTRONIK = 1,
        SEVCON = 2,
        CURTIS = 3
    };

  protected:
    const typecode type;
};

class ControllerFactory
{
  public:
    /**
     * Create motor controller based on type code and CAN id
     */
    static std::unique_ptr<Controller> create(uint8_t type, canid_t canid);
    /**
     * Create motor controller based on XML element
     */    
    static std::unique_ptr<Controller> create(tinyxml2::XMLElement *elem);
};

} // namespace ovutil