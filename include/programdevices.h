#pragma once

int save_config(const char *ifname, const char *fn);
int write_config(const char *ifname, const char *fn);
int set_canid(const char *ifname, const char *oldid_str, const char *newid_str);
int reset_throttle_calibration(const char *ifname, const char *id_str);
