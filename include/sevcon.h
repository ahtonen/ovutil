#pragma once
#include "controller.h"

namespace ovutil
{

class Sevcon : public Controller
{
  public:
    Sevcon(canid_t id);
    ~Sevcon();

    void update(const struct can_frame &frame);
};

} // namespace ovutil