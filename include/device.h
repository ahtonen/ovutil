#pragma once
#include <linux/can.h>
#include <string>
#include <memory>
#include <vector>
#include <chrono>
#include "tinyxml2.h"

namespace ovutil
{
/**
 * Abstract base class for CAN devices
 */
class Device
{
  public:
    /**
     * Timeout in seconds
     */
    Device(canid_t id, const std::string &name, double timeout);
    virtual ~Device() {}
    /**
     * Update device settings by parsing its CAN messages
     */
    virtual void update(const struct can_frame &frame) = 0;
    /**
     * Return CAN id
     */
    canid_t getId() const;
    /**
     * Return CAN id as hex string
     */
    std::string getIdString() const;
    /**
     * Return device name
     */
    std::string getName() const;
    /**
     * Check if device is online
     */
    bool isOnline() const;

  protected:
    /**
     * Update timestamp
     */
    void poke();
    const canid_t id;
    const std::string name;

  private:
    double timeout;
    std::chrono::system_clock::time_point lastUpdateTime;
};

namespace oceanvolt
{
/**
 * Abstract base class for Oceanvolt CAN devices
 */
class Device : public ovutil::Device
{
  public:
    enum typecode : uint8_t;

    Device(canid_t id, typecode t, const std::string &name, double timeout);
    virtual ~Device() {}
    /**
     * Print all settings and their values
     */
    virtual void printSettings() const = 0;
    /**
     * Create XML element having current parameter values
     */
    virtual tinyxml2::XMLElement *getXMLElement(tinyxml2::XMLDocument &doc) const = 0;
    /**
     * Get CAN configuration frames for writing object's parameter values to actual physical device.
     * Note: does NOT include changing CAN id (see below).
     */
    virtual std::unique_ptr<std::vector<struct can_frame>> getConfigChangeFrames() const = 0;
    /**
     * Print one liner for name, CAN ID, SW version (base ID msg info) and
     * elapsed time since last message received    
     */
    void printStatus() const;
    /**
     * Detect Oceanvolt Display device
     */
    static bool isDisplay(const struct can_frame&);
    static bool isDisplay(const std::string&);
    /**
     * Detect Oceanvolt Throttle device
     */
    static bool isThrottle(const struct can_frame&);
    static bool isThrottle(const std::string&);
    /**
     * Detect valid Oceanvolt device CAN ID
     */
    static bool isValidCANID(canid_t canid);
    /**
     * Conversion utils between type codes and type strings
     */
    static std::string typeToString(typecode t);
    static typecode stringToType(const std::string &str);
    /**
     * Construct CAN frame for changing given device's CAN ID.
     */
    static struct can_frame getCANIDChangeFrame(canid_t oldId, canid_t newId);
    /**
     * Oceanvolt device type codes
     */
    enum typecode : uint8_t
    {
        OCEANVOLT_THROTTLE = 1,
        OCEANVOLT_DISPLAY = 2
    };

  protected:
    /**
     * Create software version string
     */
    void parseSoftwareVersion(uint8_t major, uint8_t minor);
    const typecode type;

  private:
    std::string softwareVersion;
};
} // namespace oceanvolt

} // namespace ovutil