#pragma once
#include "controller.h"

namespace ovutil
{

class Piktronik : public Controller
{
  public:
    Piktronik(canid_t id);
    ~Piktronik();

    void update(const struct can_frame &frame);
};

} // namespace ovutil