#pragma once
#include <cstdint>
#include "device.h"
#include "controller.h"

namespace ovutil
{
namespace oceanvolt
{

class Throttle : public Device
{
  public:
    Throttle(canid_t id, int16_t rpmFullAhead, int16_t rpmFullAstern, std::unique_ptr<Controller>);
    Throttle(canid_t id);
    //Throttle(tinyxml2::XMLElement*)
    ~Throttle();
    /**
     * Update device settings by parsing its CAN messages
     */
    void update(const struct can_frame &frame);
    /**
     * Print all settings and their values
     */
    void printSettings() const;
    /**
     * Create XML element having current parameter values
     */
    tinyxml2::XMLElement *getXMLElement(tinyxml2::XMLDocument &) const;
    /**
     * Get CAN configuration frames for writing object's parameter values to actual physical device
     */
    std::unique_ptr<std::vector<struct can_frame>> getConfigChangeFrames() const;
    /**
     * Get CAN frame for resetting calibration of given throttle device
     */
    static struct can_frame getCalResetFrame(canid_t id);

  private:
    void parseStatus(uint8_t value);
    void parseCalibrationStatus(uint8_t value);

    std::string status;
    std::string calibrationStatus;

    const int16_t rpmFullAhead;
    const int16_t rpmFullAstern;
    std::unique_ptr<Controller> motorController;
};

class ThrottleFactory
{
  public:
    /**
     * Create Throttle object from XML element
     */
    static std::shared_ptr<Throttle> create(tinyxml2::XMLElement *);
};

} // namespace oceanvolt

} // namespace ovutil