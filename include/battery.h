#pragma once
//#include <cstdint>
#include "device.h"

namespace ovutil
{

/**
 * Abstract base class for batteries
 */
class Battery : public Device
{
  public:
    enum typecode : uint8_t;

    Battery(canid_t id, typecode t, const std::string &name, uint16_t capacity);
    virtual ~Battery() {}
    /**
     * Get type code and string
     */
    typecode getType() const;
    std::string getTypeString() const;
    /**
     * Return battery capacity (Ah)
     */
    uint16_t getCapacity() const;
    /**
     * Print all settings and their values
     */
    virtual void printSettings() const = 0;
    /**
     * Save current parameters as XML elements under parent
     */
    void saveAsXML(tinyxml2::XMLElement *parent, tinyxml2::XMLDocument &doc) const;
    /**
     * Conversion utils between type codes and type strings
     */
    static std::string typeToString(typecode t);
    static typecode stringToType(const std::string &str);
    /**
     * Battery type codes
     */
    enum typecode : uint8_t
    {
        VALENCE = 1,
        VICTRON = 2
    };

  private:
    const typecode type;
    const uint16_t capacity;
};

class BatteryFactory
{
  public:
    /**
     * Create battery based on type code
     */
    static std::unique_ptr<Battery> create(uint8_t type, canid_t canid, uint16_t capacity);
    /**
     * Create battery based on XML element
     */
    static std::unique_ptr<Battery> create(tinyxml2::XMLElement *elem);
};

} // namespace ovutil