#pragma once
#include "battery.h"

namespace ovutil
{

class Victron : public Battery
{
  public:
    Victron(const canid_t id, const uint16_t capacity);
    ~Victron() {};

    void update(const struct can_frame &frame);
    void printSettings() const;
};

} // namespace ovutil