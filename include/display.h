#pragma once
#include <cstdint>
#include "device.h"
#include "controller.h"
#include "battery.h"

namespace ovutil
{
namespace oceanvolt
{

class Display : public Device
{
  public:
    Display(canid_t id,
            bool regenerationEnabled,
            uint8_t batteryLevelAtRegenerationStop,
            uint16_t r1,
            uint16_t r2,
            uint16_t r3,
            uint16_t r4,
            uint16_t r5,
            uint16_t r6,
            uint32_t r7,
            std::unique_ptr<Controller> c,
            std::unique_ptr<Battery> b);
    Display(canid_t id);
    ~Display();
    /**
     * Update device settings by parsing its CAN messages
     */
    void update(const struct can_frame &frame);
    /**
     * Print all settings and their values
     */
    void printSettings() const;
    /**
     * Create XML element having current parameter values
     */
    tinyxml2::XMLElement *getXMLElement(tinyxml2::XMLDocument &doc) const;
    /**
     * Get CAN configuration frames for writing object's parameter values to actual physical device
     */
    std::unique_ptr<std::vector<struct can_frame>> getConfigChangeFrames() const;
    /**
     * Construct CAN frame for querying register values in actual physical device
     */
    void readRegister(struct can_frame &frame, u_int8_t no) const;    
    /**
     * Number of configuration messages (not counting the message changing device's can id)
     */
    static constexpr uint8_t NO_CONFIG_MESSAGES = 5;

  private:
    std::string status;
    std::string calibrationStatus;

    bool regenerationEnabled;
    uint8_t batteryLevelAtRegenerationStop;
    uint16_t u16regenerationParam[6];
    uint32_t u32regenerationParam;

    std::unique_ptr<Controller> motorController;
    std::unique_ptr<Battery> battery;
};

class DisplayFactory
{
  public:
    /**
     * Create Display object from XML element
     */
    static std::shared_ptr<Display> create(tinyxml2::XMLElement *);
};

} // namespace oceanvolt

} // namespace ovutil