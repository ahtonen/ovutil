#pragma once
#include <linux/can.h>
#include <vector>
#include <string>
#include <memory>
#include "device.h"

namespace ovutil
{
namespace oceanvolt
{
/**
 * Abstraction for a device assembly connected to same CAN bus
 */
class Assembly
{
  public:
    Assembly();
    ~Assembly();
    /**
     * Check if there's device existing with given CAN ID
     */
    bool has(canid_t id) const;
    bool empty() const;
    /**
     * Add device
     */
    void add(std::shared_ptr<oceanvolt::Device> dev);
    /**
     * Update devices
     */
    void update(const struct can_frame &frame);
    /**
     * Print one-liner status from all devices
     */
    void printStatus() const;
    /**
     * Print all settings from all devices
     */
    void printSettings() const;
    void printSettings(canid_t id) const;
    /**
     * Save configuration to file (in XML format)
     */
    void save(const char *fn) const;
    /**
     * Write configuration to CAN bus
     */
    void write(int socket) const;

  private:
    std::vector<std::shared_ptr<oceanvolt::Device>> devices;
};

class AssemblyFactory
{
  public:
    /**
     * Create boat's assembly of devices by parsing information from CAN bus
     */
    static std::unique_ptr<Assembly> createFromSocket(int socket);
    /**
     * Create boat's assembly of devices by parsing configuration file
     */
    static std::unique_ptr<Assembly> createFromConfigFile(const char *fn);
    /**
     * Time in seconds to parse information from CAN bus
     */
    static constexpr double POLLING_TIME = 5.0;
};

} // namespace oceanvolt

} // namespace ovutil