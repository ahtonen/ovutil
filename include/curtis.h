#pragma once
#include "controller.h"

namespace ovutil
{

class Curtis : public Controller
{
  public:
    Curtis(canid_t id);
    ~Curtis();

    void update(const struct can_frame &frame);
};

} // namespace ovutil