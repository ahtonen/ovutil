#pragma once
#include "battery.h"

namespace ovutil
{

class Valence : public Battery
{
  public:
    Valence(const canid_t id, const uint16_t capacity);
    ~Valence() {};
    
    void update(const struct can_frame &frame);
    void printSettings() const;
};

} // namespace ovutil