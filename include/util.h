#pragma once
#include <memory>
#include <sstream>
#include <linux/can.h>
#include "tinyxml2.h"

namespace ovutil
{
/**
 * Helper function for formatted strings. NOTE that implementation must be here in header file.
 */
template <typename... Args>
std::string string_format(const std::string &format, Args... args)
{
    size_t size = 1 + snprintf(nullptr, 0, format.c_str(), args...);

    std::unique_ptr<char[]> buf(new char[size]);
    snprintf(buf.get(), size, format.c_str(), args...);

    return std::string(buf.get(), buf.get() + size);
}
/**
 * Pack given CAN id to first four bytes starting from `dst`.
 * Read CAN id from four bytes starting from byte `src`.
 *
 * The id is saved in big-endian format. (c)Aleksi.
 */
void packCANID(canid_t id, uint8_t *dst);
canid_t unpackCANID(const uint8_t *src);
/**
 * Pack and unpack uint16. (c)Aleksi.
 */
void packuint16(uint16_t x, uint8_t *dst);
uint16_t unpackuint16(const uint8_t *src);
/**
 * Compute CRC8. (c)Aleksi.
 */
uint8_t crc8(const uint8_t *table, size_t n);
/**
 * Pack int16
 */
void packint16(int16_t x, uint8_t *dst);
/**
 * Pack and unpack uint32.
 */
void packuint32(uint32_t x, uint8_t *dst);
uint32_t unpackuint32(const uint8_t *src);
/**
 * Socket CAN interface utils.
 */
int create_canbus_socket(const char *ifname);
void read_canbus_socket(int socket, struct can_frame *frame);
void write_canbus_socket(int socket, const struct can_frame &frame);
int close_canbus_socket(int socket);
/**
 * Convert hex string to CAN id within EFF address range.
 */
void hexstr2canid(const char *str, canid_t *id);
/**
 * Convert CAN id to hex string within EFF address range.
 */
std::string canid2hexstr(canid_t id);
/**
 * Parse CAN id and device type attributes from XML element
 */
canid_t parseIdAttribute(tinyxml2::XMLElement *elem);
std::string parseTypeAttribute(tinyxml2::XMLElement *elem);
/**
 * Parse and range check uint16 and int16 attribute from XML element
 */
uint16_t parseUint16Attribute(tinyxml2::XMLElement *elem, const char *attribute);
int16_t parseInt16Attribute(tinyxml2::XMLElement *elem, const char *attribute);
/**
 * Parse and range check value
 */
uint8_t parseUint8Value(tinyxml2::XMLElement *parent, const char* name);
uint16_t parseUint16Value(tinyxml2::XMLElement *parent, const char* name);
uint32_t parseUint32Value(tinyxml2::XMLElement *parent, const char* name);




} // namespace ovutil
