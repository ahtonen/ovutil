#include <iostream>
#include <sstream>
#include <memory>
#include "battery.h"
#include "valence.h"
#include "victron.h"
#include "util.h"

namespace ovutil
{

// Use 5.0s timeout for all batteries
Battery::Battery(canid_t id, typecode t, const std::string &name, uint16_t capacity) : Device(id, name, 5.0),
                                                                                       type(t),
                                                                                       capacity(capacity)
{
}

Battery::typecode
Battery::getType() const
{
    return type;
}

std::string
Battery::getTypeString() const
{
    return typeToString(type);
}

uint16_t
Battery::getCapacity() const
{
    return capacity;
}

void Battery::saveAsXML(tinyxml2::XMLElement *parent, tinyxml2::XMLDocument &doc) const
{
    std::ostringstream ss;

    // Comment on valid type names
    ss << "Valid types: " << typeToString(VALENCE) << ", " << typeToString(VICTRON) << ". Capacity in Ah.";
    parent->InsertEndChild(doc.NewComment(ss.str().c_str()));

    // Battery element
    tinyxml2::XMLElement *battery = doc.NewElement("Battery");
    battery->SetAttribute("id", getIdString().c_str());
    battery->SetAttribute("type", getTypeString().c_str());
    battery->SetAttribute("capacity", capacity);
    parent->InsertEndChild(battery);
}

std::string
Battery::typeToString(typecode t)
{
    if (t == VALENCE)
    {
        return "valence";
    }
    else if (t == VICTRON)
    {
        return "victron";
    }
    else
    {
        throw std::invalid_argument("Unknown Battery type code.");
    }
}

Battery::typecode
Battery::stringToType(const std::string &str)
{
    if (str == "valence")
    {
        return VALENCE;
    }
    else if (str == "victron")
    {
        return VICTRON;
    }
    else
    {
        std::ostringstream ss;
        ss << "Invalid battery type string: " << str << ". Valid types: ";
        ss << typeToString(VALENCE) << ", ";
        ss << typeToString(VICTRON) << ".";
        
        throw std::invalid_argument(ss.str());
    }
}

std::unique_ptr<Battery>
BatteryFactory::create(uint8_t type, canid_t canid, uint16_t capacity)
{
    std::unique_ptr<Battery> battery;

    if (type == Battery::VALENCE)
    {
        battery = std::make_unique<Valence>(canid, capacity);
    }
    else if (type == Battery::VICTRON)
    {
        battery = std::make_unique<Victron>(canid, capacity);
    }
    else
    {
        std::ostringstream ss;
        ss << "Invalid battery 'type' argument: " << type << ". Valid types: ";
        ss << Battery::VALENCE << ", ";
        ss << Battery::VICTRON << ".";

        throw std::invalid_argument(ss.str());
    }

    return battery;
}

std::unique_ptr<Battery>
BatteryFactory::create(tinyxml2::XMLElement *elem)
{
    canid_t id = parseIdAttribute(elem);
    Battery::typecode t = Battery::stringToType(parseTypeAttribute(elem));
    uint16_t c = parseUint16Attribute(elem, "capacity");

    return create(t, id, c);
}

} // namespace ovutil