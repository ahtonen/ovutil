#include <iostream>
#include <cstring>
#include <climits>
#include <unistd.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/uio.h>
#include "util.h"
#include "assert.h"

namespace ovutil
{
/**
 * Compute one round of Maxim iButton CRC8
 *
 * Polynomial: x^8 + x^5 + x^4 + 1 (0x8C)
 */
static uint8_t crc8_round(uint8_t crc, uint8_t data);

void packCANID(canid_t id, uint8_t *dst)
{
    dst[0] = (id >> 24) & 0xFF;
    dst[1] = (id >> 16) & 0xFF;
    dst[2] = (id >> 8) & 0xFF;
    dst[3] = id & 0xFF;
}

canid_t unpackCANID(const uint8_t *src)
{
    canid_t id = src[0];
    id = (id << 8) + src[1];
    id = (id << 8) + src[2];
    id = (id << 8) + src[3];
    return id;
}

void packuint16(uint16_t x, uint8_t *dst)
{
    dst[0] = x >> 8;
    dst[1] = x & 0xFF;
}

uint16_t unpackuint16(const uint8_t *src)
{
    uint16_t x = src[0] & 0xFF;
    return (x << 8) + (src[1] & 0xFF);
}

void packint16(int16_t x, uint8_t *dst)
{
    dst[0] = x >> 8;
    dst[1] = x & 0xFF;
}

void packuint32(uint32_t x, uint8_t *dst)
{
    dst[0] = (x >> 24) & 0xFF;
    dst[1] = (x >> 16) & 0xFF;
    dst[2] = (x >> 8) & 0xFF;
    dst[3] = x & 0xFF;
}

uint32_t unpackuint32(const uint8_t *src)
{
    uint32_t x = src[0];
    x = (x << 8) + src[1];
    x = (x << 8) + src[2];
    x = (x << 8) + src[3];
    return x;
}

uint8_t crc8(const uint8_t *table, size_t n)
{
    uint8_t crc = 0;
    for (size_t i = 0; i < n; ++i)
        crc = crc8_round(crc, table[i]);
    return crc;
}

static uint8_t crc8_round(uint8_t crc, uint8_t data)
{
    crc ^= data;
    for (int i = 0; i < 8; ++i)
    {
        if (crc & 0x01)
            crc = (crc >> 1) ^ 0x8C;
        else
            crc >>= 1;
    }
    return crc;
}

int create_canbus_socket(const char *ifname)
{
    int s;
    struct ifreq ifr;
    struct sockaddr_can addr;

    /* Open the CAN network interface */
    if ((s = ::socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0)
    {
        std::perror("Error while opening socket");
        return -1;
    }

    /* Get the index of the network interface */
    std::strncpy(ifr.ifr_name, ifname, IF_NAMESIZE);
    if (::ioctl(s, SIOCGIFINDEX, &ifr) < 0)
    {
        std::perror("SIOCGIFINDEX");
        return -1;
    }

    /* Bind the socket to the network interface */
    addr.can_family = AF_CAN;
    addr.can_ifindex = ifr.ifr_ifindex;
    if (::bind(s,
               reinterpret_cast<struct sockaddr *>(&addr),
               sizeof(addr)) < 0)
    {
        std::perror("Error in socket bind");
        return -1;
    }

#ifdef DEBUG
    fprintf(stdout, "%s at index %d\n", ifname, ifr.ifr_ifindex);
#endif

    return s;
}

void read_canbus_socket(int socket, struct can_frame *frame)
{
    int nbytes;

    nbytes = ::read(socket, frame, sizeof(struct can_frame));

    if (nbytes < 0)
    {
        std::perror("can raw socket read");
        throw std::runtime_error("can raw socket read");
    }

    if ((unsigned int)nbytes < sizeof(struct can_frame))
    {
        throw std::runtime_error("read: incomplete CAN frame");
    }
}

void write_canbus_socket(int socket, const struct can_frame &frame)
{
    int nbytes;

    nbytes = ::write(socket, &frame, sizeof(struct can_frame));

    if (nbytes < 0)
    {
        std::perror("can raw socket write");
        throw std::runtime_error("can raw socket write");
    }
    else if ((unsigned int)nbytes < sizeof(struct can_frame))
    {
        throw std::runtime_error("write: incomplete CAN frame");
    }
}

int close_canbus_socket(int socket)
{
    int res;

    if ((res = ::close(socket)) < 0)
    {
        std::perror("Error while closing socket");
    }

    return res;
}

void hexstr2canid(const char *str, canid_t *id)
{
    long val;
    char *endptr;

    errno = 0;

    if (str == '\0')
    {
        throw std::invalid_argument("Assumed hexadecimal string, but given null string.");
    }

    // Try to convert string to long
    val = strtol(str, &endptr, 16);
    if ((errno == ERANGE && (val == LONG_MAX || val == LONG_MIN)) || (errno != 0 && val == 0))
    {
        std::perror("strtol");
        throw std::runtime_error("strtol");
    }

    if (*endptr != '\0')
    {
        throw std::invalid_argument("Hexadecimal string contains invalid characters.");
    }

    if (val < 0 || val > (signed)CAN_EFF_MASK)
    {
        throw std::runtime_error(string_format("Value out of EFF address range. Should be within 0x0-0x%08X", CAN_EFF_MASK));
    }

    *id = (canid_t)val;
}

std::string canid2hexstr(canid_t id)
{
    // id should always be within EFF range
    assert(id <= CAN_EFF_MASK);

    return string_format("0x%08X", id);
}

canid_t parseIdAttribute(tinyxml2::XMLElement *elem)
{
    canid_t id;
    const char *idString = elem->Attribute("id");

    if (idString == nullptr)
    {
        throw std::runtime_error("Attribute 'id' not found.");
    }

    hexstr2canid(idString, &id);

    return id;
}

std::string parseTypeAttribute(tinyxml2::XMLElement *elem)
{
    const char *typeString = elem->Attribute("type");

    if (typeString == nullptr)
    {
        throw std::runtime_error("Attribute 'type' not found.");
    }

    return std::string(typeString);
}

uint16_t parseUint16Attribute(tinyxml2::XMLElement *elem, const char *attribute)
{
    using namespace tinyxml2;

    int i;
    XMLError err = elem->QueryIntAttribute(attribute, &i);

    if (err != XML_SUCCESS)
    {
        std::ostringstream ss;
        ss << "Failed to parse attribute '" << attribute << "' value to integer.";
        throw std::runtime_error(ss.str());
    }
    if (i < 0 || i > UINT16_MAX)
    {
        std::ostringstream ss;
        ss << "Attribute '" << attribute << "' value: " << i << " out of uint16 range.";
        throw std::out_of_range(ss.str());
    }

    return static_cast<uint16_t>(i);
}

int16_t parseInt16Attribute(tinyxml2::XMLElement *elem, const char *attribute)
{
    using namespace tinyxml2;

    int i;
    XMLError err = elem->QueryIntAttribute(attribute, &i);

    if (err != XML_SUCCESS)
    {
        std::ostringstream ss;
        ss << "Failed to parse attribute '" << attribute << "' value to integer.";
        throw std::runtime_error(ss.str());
    }
    if (i < INT16_MIN || i > INT16_MAX)
    {
        std::ostringstream ss;
        ss << "Attribute '" << attribute << "' value: " << i << " out of int16 range.";
        throw std::out_of_range(ss.str());
    }

    return static_cast<uint16_t>(i);
}

uint8_t parseUint8Value(tinyxml2::XMLElement *parent, const char *name)
{
    using namespace tinyxml2;

    XMLElement *elem = parent->FirstChildElement(name);
    if (elem == nullptr)
    {
        std::ostringstream ss;
        ss << "Child element '" << name << "' not found.";
        throw std::runtime_error(ss.str());
    }

    int i;
    XMLError err = elem->QueryIntText(&i);

    if (err != XML_SUCCESS)
    {
        std::ostringstream ss;
        ss << "Failed to parse '" << name << "' element's value to integer.";
        throw std::runtime_error(ss.str());
    }

    if (i < 0 || i > UINT8_MAX)
    {
        std::ostringstream ss;
        ss << "'" << name << "' element's value: " << i << " out of uint8 range.";
        throw std::out_of_range(ss.str());
    }

    return static_cast<uint8_t>(i);
}

uint16_t parseUint16Value(tinyxml2::XMLElement *parent, const char *name)
{
    using namespace tinyxml2;

    XMLElement *elem = parent->FirstChildElement(name);
    if (elem == nullptr)
    {
        std::ostringstream ss;
        ss << "Child element '" << name << "' not found.";
        throw std::runtime_error(ss.str());
    }

    int i;
    XMLError err = elem->QueryIntText(&i);

    if (err != XML_SUCCESS)
    {
        std::ostringstream ss;
        ss << "Failed to parse '" << name << "' element's value to integer.";
        throw std::runtime_error(ss.str());
    }

    if (i < 0 || i > UINT16_MAX)
    {
        std::ostringstream ss;
        ss << "'" << name << "' element's value: " << i << " out of uint16 range.";
        throw std::out_of_range(ss.str());
    }

    return static_cast<uint16_t>(i);
}

uint32_t parseUint32Value(tinyxml2::XMLElement *parent, const char *name)
{
    using namespace tinyxml2;

    XMLElement *elem = parent->FirstChildElement(name);
    if (elem == nullptr)
    {
        std::ostringstream ss;
        ss << "Child element '" << name << "' not found.";
        throw std::runtime_error(ss.str());
    }

    int i;
    XMLError err = elem->QueryIntText(&i);

    if (err != XML_SUCCESS)
    {
        std::ostringstream ss;
        ss << "Failed to parse '" << name << "' element's value to integer.";
        throw std::runtime_error(ss.str());
    }
    if (i < 0)
    {
        std::ostringstream ss;
        ss << "'" << name << "' element's value: " << i << " out of uint32 range.";
        throw std::out_of_range(ss.str());
    }
    else if ((unsigned)i > UINT32_MAX)
    {
        std::ostringstream ss;
        ss << "'" << name << "' element's value: " << i << " out of uint16 range.";
        throw std::out_of_range(ss.str());
    }

    return static_cast<uint32_t>(i);
}

} // namespace ovutil