/*
 * canbusload.c
 *
 * Copyright (c) 2002-2008 Volkswagen Group Electronic Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of Volkswagen nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * Alternatively, provided that this notice is retained in full, this
 * software may be distributed under the terms of the GNU General
 * Public License ("GPL") version 2, in which case the provisions of the
 * GPL apply INSTEAD OF those given above.
 *
 * The provided data structures and external interfaces from this code
 * are not restricted to be used by modules with a GPL compatible license.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 *
 * Send feedback to <linux-can@vger.kernel.org>
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <libgen.h>
#include <time.h>
#include <signal.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/uio.h>
#include <net/if.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#include "terminal.h"
#include "canframelen.h"
#include "canbusload.h"

#define MAXSOCK 16 /* max. number of CAN interfaces given on the cmdline */

#define PERCENTRES 5			  /* resolution in percent for bargraph */
#define NUMBAR (100 / PERCENTRES) /* number of bargraph elements */

extern int running;
static struct
{
	char devname[IFNAMSIZ + 1];
	unsigned int bitrate;
	unsigned int recv_frames;
	unsigned int recv_bits_total;
	unsigned int recv_bits_payload;
} stat[MAXSOCK + 1];

static unsigned int max_devname_len; /* to prevent frazzled device name output */
static unsigned int max_bitrate_len;
static unsigned int currmax;
static unsigned char redraw;
static unsigned char timestamp;
static unsigned char color;
static unsigned char bargraph;
static enum cfl_mode mode = CFL_WORSTCASE;

static void printstats(int signo)
{
	unsigned int i, j, percent;

	if (redraw)
		printf("%s", CSR_HOME);

	if (timestamp)
	{
		time_t currtime;
		struct tm now;

		if (time(&currtime) == (time_t)-1)
		{
			perror("time");
			exit(1);
		}

		localtime_r(&currtime, &now);

		printf("%04d-%02d-%02d %02d:%02d:%02d ",
			   now.tm_year + 1900,
			   now.tm_mon + 1,
			   now.tm_mday,
			   now.tm_hour,
			   now.tm_min,
			   now.tm_sec);

		switch (mode)
		{

		case CFL_NO_BITSTUFFING:
			/* plain bit calculation without bitstuffing */
			printf("(ignore bitstuffing)\n");
			break;

		case CFL_WORSTCASE:
			/* worst case estimation - see above */
			printf("(worst case bitstuffing)\n");
			break;

		case CFL_EXACT:
			/* exact calculation of stuffed bits based on frame content and CRC */
			printf("(exact bitstuffing)\n");
			break;

		default:
			printf("(unknown bitstuffing)\n");
			break;
		}
	}

	for (i = 0; i < currmax; i++)
	{
		if (color)
		{
			if (i % 2)
				printf("%s", FGRED);
			else
				printf("%s", FGBLUE);
		}

		if (stat[i].bitrate)
			percent = (stat[i].recv_bits_total * 100) / stat[i].bitrate;
		else
			percent = 0;

		printf(" %*s@%-*d %5d %7d %6d %3d%%",
			   max_devname_len, stat[i].devname,
			   max_bitrate_len, stat[i].bitrate,
			   stat[i].recv_frames,
			   stat[i].recv_bits_total,
			   stat[i].recv_bits_payload,
			   percent);

		if (bargraph)
		{

			printf(" |");

			if (percent > 100)
				percent = 100;

			for (j = 0; j < NUMBAR; j++)
			{
				if (j < percent / PERCENTRES)
					printf("X");
				else
					printf(".");
			}

			printf("|");
		}

		if (color)
			printf("%s", ATTRESET);

		printf("\n");

		stat[i].recv_frames = 0;
		stat[i].recv_bits_total = 0;
		stat[i].recv_bits_payload = 0;
	}

	printf("\n");
	fflush(stdout);

	alarm(1);
}

int monitor_load(const char *ifname)
{
	fd_set rdfs;
	int s[MAXSOCK];

	char *nptr;
	struct sockaddr_can addr;
	struct can_frame frame;
	unsigned int nbytes, i;
	struct ifreq ifr;
	sigset_t sigmask, savesigmask;

	signal(SIGALRM, printstats);

	/* redraw terminal window */
	redraw = 1;
	/* show timestamp */
	timestamp = 1;
	/* use bar graph */
	bargraph = 1;
	/* use default worst case bitstuffing */
	/* mode = CFL_NO_BITSTUFFING;
	mode = CFL_EXACT; */
	/* number of CAN interfaces */
	currmax = 1;

	if (currmax > MAXSOCK)
	{
		fprintf(stderr, "More than %d CAN devices given!\n", MAXSOCK);
		return 1;
	}

	for (i = 0; i < currmax; i++)
	{
		nbytes = strlen(ifname);
		if (nbytes >= IFNAMSIZ + sizeof("@1000000") + 1)
		{
			fprintf(stderr, "Name of CAN device '%s' is too long!\n", ifname);
			return 1;
		}

		s[i] = socket(PF_CAN, SOCK_RAW, CAN_RAW);
		if (s[i] < 0)
		{
			perror("socket");
			return 1;
		}

		nptr = strchr((char *)ifname, '@');

		if (!nptr)
		{
			perror("CAN device name invalid. Should be e.g. 'can0@250000'.");
			return 1;
		}

		nbytes = nptr - ifname; /* interface name is up the first '@' */

		if (nbytes >= IFNAMSIZ)
		{
			fprintf(stderr, "Name of CAN device '%s' is too long!\n", ifname);
			return 1;
		}

		strncpy(stat[i].devname, ifname, nbytes);
		memset(&ifr.ifr_name, 0, sizeof(ifr.ifr_name));
		strncpy(ifr.ifr_name, ifname, nbytes);

		if (nbytes > max_devname_len)
			max_devname_len = nbytes; /* for nice printing */

		stat[i].bitrate = atoi(nptr + 1); /* bitrate is placed behind the '@' */

		if (!stat[i].bitrate || stat[i].bitrate > 1000000)
		{
			fprintf(stderr, "Invalid bitrate for CAN device '%s'!\n", ifname);
			return 1;
		}

		nbytes = strlen(nptr + 1);
		if (nbytes > max_bitrate_len)
			max_bitrate_len = nbytes; /* for nice printing */

#ifdef DEBUG
		printf("using interface name '%s'.\n", ifr.ifr_name);
#endif

		if (ioctl(s[i], SIOCGIFINDEX, &ifr) < 0)
		{
			perror("SIOCGIFINDEX");
			exit(1);
		}

		addr.can_family = AF_CAN;
		addr.can_ifindex = ifr.ifr_ifindex;

		if (bind(s[i], (struct sockaddr *)&addr, sizeof(addr)) < 0)
		{
			perror("bind");
			return 1;
		}
	}

    printf("Starting CAN bus load test (hit Ctrl-C to break)...\n");
    usleep(500000);

	alarm(1);

	if (redraw)
		printf("%s", CLR_SCREEN);

	while (running)
	{
		FD_ZERO(&rdfs);
		for (i = 0; i < currmax; i++)
			FD_SET(s[i], &rdfs);

		savesigmask = sigmask;

		if (pselect(s[currmax - 1] + 1, &rdfs, NULL, NULL, NULL, &sigmask) < 0)
		{
			//perror("pselect");
			sigmask = savesigmask;
			continue;
		}

		for (i = 0; i < currmax; i++)
		{ /* check all CAN RAW sockets */

			if (FD_ISSET(s[i], &rdfs))
			{

				nbytes = read(s[i], &frame, sizeof(frame));

				if (nbytes < 0)
				{
					perror("read");
					return 1;
				}

				if (nbytes < sizeof(struct can_frame))
				{
					fprintf(stderr, "read: incomplete CAN frame\n");
					return 1;
				}

				stat[i].recv_frames++;
				stat[i].recv_bits_payload += frame.can_dlc * 8;
				stat[i].recv_bits_total += can_frame_length((struct canfd_frame *)&frame,
															mode, sizeof(frame));
			}
		}
	}

	for (i = 0; i < currmax; i++)
		close(s[i]);

	return 0;
}
