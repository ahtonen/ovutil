#include "victron.h"

namespace ovutil
{

Victron::Victron(const canid_t id,
                 const uint16_t capacity) : Battery(id,
                                                    VICTRON,
                                                    "Victron battery",
                                                    capacity)
{
}

void Victron::update(const struct can_frame &frame)
{
    if (frame.can_id == getId())
    {
        poke();
    }
}

void Victron::printSettings() const
{
}

} // namespace ovutil