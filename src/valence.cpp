#include "valence.h"

namespace ovutil
{

Valence::Valence(const canid_t id,
                 const uint16_t capacity) : Battery(id,
                                                    VALENCE,
                                                    "Valence battery",
                                                    capacity)
{
}

void Valence::update(const struct can_frame &frame)
{
    if (frame.can_id == getId())
    {
        poke();
    }
}

void Valence::printSettings() const
{
}

} // namespace ovutil