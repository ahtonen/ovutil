#include <iostream>
#include <sstream>
#include "controller.h"
#include "piktronik.h"
#include "curtis.h"
#include "sevcon.h"
#include "util.h"

namespace ovutil
{

// Use 5.0s timeout for all motor controllers
Controller::Controller(canid_t id, typecode t, const std::string &name) : Device(id, name, 5.0),
                                                                          type(t)
{
}

Controller::typecode
Controller::getType() const
{
    return type;
}

std::string
Controller::getTypeString() const
{
    return typeToString(type);
}

void Controller::saveAsXML(tinyxml2::XMLElement *parent, tinyxml2::XMLDocument &doc) const
{
    std::ostringstream ss;

    // Comment on valid type names
    ss << "Valid types: " << typeToString(CURTIS) << ", " << typeToString(SEVCON) << ", " << typeToString(PIKTRONIK);
    parent->InsertEndChild(doc.NewComment(ss.str().c_str()));

    // Controller element
    tinyxml2::XMLElement *controller = doc.NewElement("Controller");
    controller->SetAttribute("id", getIdString().c_str());
    controller->SetAttribute("type", getTypeString().c_str());
    parent->InsertEndChild(controller);
}

std::string
Controller::typeToString(typecode t)
{
    if (t == PIKTRONIK)
    {
        return "piktronik";
    }
    else if (t == SEVCON)
    {
        return "sevcon";
    }
    else if (t == CURTIS)
    {
        return "curtis";
    }
    else
    {
        throw std::invalid_argument("Unknown Controller type code.");
    }
}

Controller::typecode
Controller::stringToType(const std::string &str)
{
    if (str == "piktronik")
    {
        return PIKTRONIK;
    }
    else if (str == "sevcon")
    {
        return SEVCON;
    }
    else if (str == "curtis")
    {
        return CURTIS;
    }
    else
    {
        std::ostringstream ss;
        ss << "Invalid Controller device type string: " << str << ". Valid types: ";
        ss << typeToString(CURTIS) << ", ";
        ss << typeToString(SEVCON) << ", ";
        ss << typeToString(PIKTRONIK) << ".";
        
        throw std::invalid_argument(ss.str());
    }
}

std::unique_ptr<Controller>
ControllerFactory::create(uint8_t type, canid_t canid)
{
    std::unique_ptr<Controller> controller;

    if (type == Controller::PIKTRONIK)
    {
        controller = std::make_unique<Piktronik>(canid);
    }
    else if (type == Controller::SEVCON)
    {
        controller = std::make_unique<Sevcon>(canid);
    }
    else if (type == Controller::CURTIS)
    {
        controller = std::make_unique<Curtis>(canid);
    }
    else
    {
        std::ostringstream ss;
        ss << "Invalid controller 'type' attribute: " << type << ". Valid types: ";
        ss << Controller::typeToString(Controller::CURTIS) << ", ";
        ss << Controller::typeToString(Controller::SEVCON) << ", ";
        ss << Controller::typeToString(Controller::PIKTRONIK) << ".";
        throw std::runtime_error(ss.str());
    }

    return controller;
}

std::unique_ptr<Controller>
ControllerFactory::create(tinyxml2::XMLElement *elem)
{
    canid_t id = parseIdAttribute(elem);
    Controller::typecode t = Controller::stringToType(parseTypeAttribute(elem));
    
    return create(t, id);
}

} // namespace ovutil