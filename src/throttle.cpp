#include <iostream>
#include <memory>
#include "throttle.h"
#include "terminal.h"
#include "util.h"

namespace ovutil
{
namespace oceanvolt
{

// Default motor RPM cal to zero unless explicitly given. Unique pointer references to nullptr.
Throttle::Throttle(canid_t id) : Throttle(id, 0, 0, nullptr)
{
}

// Use 2.0s timeout
Throttle::Throttle(canid_t id,
                   int16_t rpmFullAhead,
                   int16_t rpmFullAstern,
                   std::unique_ptr<Controller> c) : Device(id, OCEANVOLT_THROTTLE, "Oceanvolt Throttle", 2.0),
                                                    status("n/a"),
                                                    calibrationStatus("n/a"),
                                                    rpmFullAhead(rpmFullAhead),
                                                    rpmFullAstern(rpmFullAstern),
                                                    motorController(std::move(c))
{
}

Throttle::~Throttle()
{
}

void Throttle::update(const struct can_frame &frame)
{
    if (frame.can_id == id)
    {
        poke();
        parseSoftwareVersion(frame.data[1], frame.data[2]);
        parseStatus(frame.data[3]);
        parseCalibrationStatus(frame.data[4]);
    }
    else if (frame.can_id == (id + 1))
    {
        poke();
        if (!motorController)
        {
            motorController = ControllerFactory::create(frame.data[4],
                                                        unpackCANID(frame.data));
        }
    }
    else if (frame.can_id == (id + 2))
    {
        poke();
        // this CAN ID is reserved for future use
    }
    else if (motorController)
    {
        motorController->update(frame);
    }
}

void Throttle::printSettings() const
{
    using namespace std;
    string blank = "          ";

    // Device online status line
    printStatus();
    // Error and calibration status
    cout << blank << status << endl;
    cout << blank << calibrationStatus << endl;
    // Motor controller
    if (motorController)
    {
        cout << blank << string_format("%s configured at 0x%08X", motorController->getName().c_str(), motorController->getId()) << std::endl;
    }
    else
    {
        cout << blank << "Motor controller status message not received." << endl;
    }
}

tinyxml2::XMLElement *
Throttle::getXMLElement(tinyxml2::XMLDocument &doc) const
{
    tinyxml2::XMLElement *device = doc.NewElement("Device");
    device->SetAttribute("id", getIdString().c_str());
    device->SetAttribute("type", oceanvolt::Device::typeToString(type).c_str());

    // Save Controller parameters
    motorController->saveAsXML(device, doc);

    // RPM element
    device->InsertEndChild(doc.NewComment("These parameters must be set manually before programming."));
    tinyxml2::XMLElement *rpm = doc.NewElement("RPMFull");
    rpm->SetAttribute("astern", rpmFullAstern);
    rpm->SetAttribute("ahead", rpmFullAhead);
    device->InsertEndChild(rpm);

    return device;
}

std::unique_ptr<std::vector<struct can_frame>>
Throttle::getConfigChangeFrames() const
{
    std::unique_ptr<std::vector<struct can_frame>> frames = std::make_unique<std::vector<struct can_frame>>();
    struct can_frame frame;

    // Motor controller config
    frame.can_id = id + 5;
    frame.can_dlc = 6;
    packCANID(motorController->getId(), frame.data);
    frame.data[4] = motorController->getType();
    frame.data[5] = crc8(frame.data, frame.can_dlc - 1);
    frames->push_back(frame);

    // RPM config
    frame.can_id = id + 6;
    frame.can_dlc = 5;    
    packint16(rpmFullAstern, frame.data);
    packint16(rpmFullAhead, &frame.data[2]);
    frame.data[4] = crc8(frame.data, frame.can_dlc - 1);
    frames->push_back(frame);

    return frames;
}

struct can_frame
Throttle::getCalResetFrame(canid_t id)
{
    struct can_frame frame;
    uint8_t data[4];

    if(isValidCANID(id) == false)
    {
        throw std::invalid_argument("Invalid Oceanvolt device CAN ID");
    }

    frame.can_id = id + 3;
    frame.can_dlc = 1;
    // Pack CAN ID to memory and compute CRC8 over it
    // Possibly a bug in Throttle SW, using id+3 as workaround that
    // seems to reset the calibration.
    packCANID(id+3, data);
    frame.data[0] = crc8(data, 4);

    return frame;
}

void Throttle::parseStatus(uint8_t value)
{
    switch (value)
    {
    case 0x00:
        status = "OK";
        break;
    case 0x01:
        status = "Lever was not in neutral position when power was turned on";
        break;
    case 0x10:
        status = "Controller not found";
        break;
    case 0x11:
        status = "Controller error";
        break;
    case 0x20:
        status = "Not calibrated";
        break;
    case 0x21:
        status = "Sensor error (most likely the magnet is too far away from the sensor)";
        break;
    default:
        status = "Unknown status";
    }
}

void Throttle::parseCalibrationStatus(uint8_t value)
{
    switch (value)
    {
    case 0x00:
        calibrationStatus = "Not calibrated";
        break;
    case 0x01:
        calibrationStatus = "Left side calibrated";
        break;
    case 0x02:
        calibrationStatus = "Right side calibrated";
        break;
    default:
        calibrationStatus = "Unknown calibration status";
    }
}

std::shared_ptr<Throttle>
ThrottleFactory::create(tinyxml2::XMLElement *dev)
{
    using namespace tinyxml2;

    XMLElement *controller, *rpm;

    controller = dev->FirstChildElement("Controller");
    if (controller == nullptr)
    {
        throw std::runtime_error("'Controller' element not found.");
    }

    rpm = dev->FirstChildElement("RPMFull");
    if (rpm == nullptr)
    {
        throw std::runtime_error("'RPMFull' element not found.");
    }

    return std::make_shared<Throttle>(parseIdAttribute(dev),
                                      parseInt16Attribute(rpm, "ahead"),
                                      parseInt16Attribute(rpm, "astern"),
                                      ControllerFactory::create(controller));
}

} // namespace oceanvolt

} // namespace ovutil