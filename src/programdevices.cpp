#include <linux/can.h>
#include <cstring>
#include <iostream>
#include <memory>

#include "programdevices.h"
#include "assembly.h"
#include "throttle.h"
#include "util.h"
#include "lib.h"

int save_config(const char *ifname, const char *fn)
{
    using namespace ovutil;

    int s;
    std::unique_ptr<oceanvolt::Assembly> boat;

    if ((s = create_canbus_socket(ifname)) < 0)
    {
        return 1;
    }

    try
    {
        boat = oceanvolt::AssemblyFactory::createFromSocket(s);
        boat->save(fn);
    }
    catch (std::exception &e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }

    if (close_canbus_socket(s) < 0)
    {
        return 1;
    }

    return 0;
}

int write_config(const char *ifname, const char *fn)
{
    using namespace ovutil;

    int s;
    std::unique_ptr<oceanvolt::Assembly> boat;

    if ((s = create_canbus_socket(ifname)) < 0)
    {
        return 1;
    }

    try
    {
        boat = oceanvolt::AssemblyFactory::createFromConfigFile(fn);
        boat->write(s);
    }
    catch (std::exception &e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }

    if (close_canbus_socket(s) < 0)
    {
        return 1;
    }

    return 0;
}

int set_canid(const char *ifname, const char *oldid_str, const char *newid_str)
{
    using namespace ovutil;

    int s;
    canid_t oldId, newId;
    struct can_frame frame;

    if ((s = create_canbus_socket(ifname)) < 0)
    {
        return 1;
    }

    try
    {
        hexstr2canid(oldid_str, &oldId);
        hexstr2canid(newid_str, &newId);

        if (oceanvolt::Device::isValidCANID(oldId) == false || oceanvolt::Device::isValidCANID(newId) == false)
        {
            return 1;
        }

        frame = oceanvolt::Device::getCANIDChangeFrame(oldId, newId);
        write_canbus_socket(s, frame);

        std::cout << string_format("CAN ID change: 0x%04X => 0x%04X. Remember to RESET DEVICE!", oldId, newId) << std::endl;
    }
    catch (std::exception &e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }

    if (close_canbus_socket(s) < 0)
    {
        return 1;
    }

    return 0;
}

int reset_throttle_calibration(const char *ifname, const char *id_str)
{
    using namespace ovutil;

    int s;
    canid_t id;
    struct can_frame frame;

    if ((s = create_canbus_socket(ifname)) < 0)
    {
        return 1;
    }

    try
    {
        hexstr2canid(id_str, &id);

        if (oceanvolt::Device::isValidCANID(id) == false)
        {
            return 1;
        }

        frame = oceanvolt::Throttle::getCalResetFrame(id);
        write_canbus_socket(s, frame);

        std::cout << string_format("Throttle device 0x%04X: calibration reset.", id) << std::endl;
    }
    catch (std::exception &e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }

    if (close_canbus_socket(s) < 0)
    {
        return 1;
    }

    return 0;
}