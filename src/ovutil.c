#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <getopt.h>
#include <libgen.h>
#include <net/if.h>

#include "canbusload.h"
#include "canbuserror.h"
#include "listdevices.h"
#include "showdevices.h"
#include "programdevices.h"

/* Executable filename and version string */
static const char prg[] = "ovutil";
static const char version_str[] = "0.3";

/* Termination condition for polling commands */
volatile sig_atomic_t running = 1;
static void sigterm(int signo)
{
    running = 0;
}

/* Settings parsed from configuration file */
struct config
{
    char ifname[IF_NAMESIZE + 1];
};

static void print_default_config(FILE *stream)
{
    fprintf(stream, "#\n# This is ovutil configuration file\n#\n\n");
    fprintf(stream, "# Set CAN interface name (default can0)\n");
    fprintf(stream, "ifname = can0\n");
}

static int read_config_file(struct config *cf)
{
    FILE *fp;
    char *home;
    char *fname;
    char line[80 + 1];
    int n_matches = 0;

    if (!(home = getenv("HOME")))
    {
        fprintf(stderr, "HOME environment variable not defined!\n");
        return -1;
    }

    /* HOME path concatenated to '/.ovutil' */
    fname = (char *)malloc(strlen(home) + 6 + 1);
    strcpy(fname, home);
    strcat(fname, "/.");
    strcat(fname, prg);

    /* Write default config in case file not exists */
    if ((fp = fopen(fname, "r")) == NULL)
    {
        if ((fp = fopen(fname, "w+")))
        {
            print_default_config(fp);
            rewind(fp);
#ifdef DEBUG
            fprintf(stdout, "Wrote default config to: %s\n", fname);
#endif
        }
        else
        {
            fprintf(stderr, "Cannot open configuration file %s for writing.\n", fname);
            perror("");
            free(fname);
            return -1;
        }
    }
    free(fname);

    /* Parse config */
    const char token[] = "ifname = ";
    while (fgets(line, 80, fp))
    {
        if (strncmp(line, token, strlen(token)) == 0)
        {
#ifdef DEBUG
            fprintf(stdout, "Match found on line: %s", line);
#endif
            n_matches++;

            if ((strlen(line) - strlen(token)) > IF_NAMESIZE)
            {
                fprintf(stderr, "Interface name in configuration file is too long (max %i characters).\n", IF_NAMESIZE);
                fclose(fp);
                return -1;
            }
            else
            {
                sscanf(line, "ifname = %s", cf->ifname);
            }
        }
    }

    if (ferror(fp))
    {
        perror("Read error.");
        fclose(fp);
        return -1;
    }

    /* Handle no matches case */
    if (n_matches == 0)
    {
        fprintf(stderr, "Invalid configuration file $HOME/.%s format. Should be like:\n\n", prg);
        print_default_config(stderr);
        fprintf(stderr, "-clip-\n\n");
        fclose(fp);
        return -1;
    }

    fclose(fp);
    return 0;
}

static void print_usage()
{

    fprintf(stdout, "Usage: %s [--version] [--help] <command> [<args>]\n\n", prg);
    fprintf(stdout, "Commands:\n");
    fprintf(stdout, "   load      Run CAN bus load test\n");
    fprintf(stdout, "   diag      Poll CAN bus for errors\n");
    fprintf(stdout, "   list      List devices found in given CAN bus\n");
    fprintf(stdout, "   show      List all devices and their parameters\n");
    fprintf(stdout, "   get       Save Oceanvolt device parameters to file\n");
    fprintf(stdout, "   set       Write Oceanvolt device parameters from file\n");
    fprintf(stdout, "   setcanid  Set Oceanvolt device CAN ID\n");
    fprintf(stdout, "   resetcal  Reset Throttle calibration\n\n");
    fprintf(stdout, "Default CAN interface is can0 unless specified otherwise in\n");
    fprintf(stdout, "configuration file '.%s'.\n\n", prg);
    fprintf(stdout, "See '%s <command> help' to read about specific subcommand.\n\n", prg);
}

static void print_version()
{
    fprintf(stdout, "Oceanvolt command line utility, version: %s\n", version_str);
}

int main(int argc, char *argv[])
{
    static struct option base_options[] =
        {
            {"help", no_argument, 0, 'h'},
            {"version", no_argument, 0, 'v'},
            {"print", no_argument, 0, 'p'},
            {"follow", no_argument, 0, 'f'},
            {"all", no_argument, 0, 'a'},
            {"id", required_argument, 0, 'i'},
            {0, 0, 0, 0}};

    static struct option setget_options[] =
        {
            {"help", no_argument, 0, 'h'},
            {"old", required_argument, 0, 'o'},
            {"new", required_argument, 0, 'n'},
            {"file", required_argument, 0, 'f'},
            {0, 0, 0, 0}};

    static struct config cf;
    int err = 0;
    /* getopt_long stores the option index here */
    int option_index = 0;
    /* used both to encode short option and -1 when all options are done */
    int c;
    /* command */
    char *cmd = argv[1];

    signal(SIGTERM, sigterm);
    signal(SIGHUP, sigterm);
    signal(SIGINT, sigterm);

    if (argc == 1)
    {
        print_usage();
        exit(0);
    }

    if (read_config_file(&cf) != 0)
    {
        exit(1);
    }

    if (strcmp("load", cmd) == 0)
    {
        while ((c = getopt_long(argc, argv, "h", base_options, &option_index)) != -1)
        {
            switch (c)
            {
            case 'h':
                fprintf(stderr, "Usage: %s %s\n\n", prg, cmd);
                fprintf(stderr, "Poll CAN bus load. Use CTRL-C to terminate.\n\n");
                fprintf(stderr, "Due to the bitstuffing estimation the calculated busload may exceed 100%%.\n");
                fprintf(stderr, "The bus load data is presented in one line which contains:\n\n");
                fprintf(stderr, "(CAN interface) (received CAN frames) (used bits total) (used bits for payload)\n");
                fprintf(stderr, "\nExample:\n");
                fprintf(stderr, "2014-02-01 21:13:16 (worst case bitstuffing)\n");
                fprintf(stderr, " can0@100000   805   74491  36656  74%% |XXXXXXXXXXXXXX......|\n");
                exit(0);
            case '?':
                fprintf(stderr, "Unknown option '-%c'. See '%s %s --help'.\n", optopt, prg, cmd);
                exit(1);
            default:
                abort();
            }
        }

        if (argc > 2)
        {
            fprintf(stderr, "Too many arguments. See '%s %s --help'.\n", prg, cmd);
            exit(1);
        }

        /* run load monitor */
        char ifname_rate[20 + 7 + 1];
        strcpy(ifname_rate, cf.ifname);
        strcat(ifname_rate, "@250000");
        err = monitor_load(ifname_rate);
    }
    else if (strcmp("diag", cmd) == 0)
    {
        int print_normal_frames = 0;

        while ((c = getopt_long(argc, argv, "hp", base_options, &option_index)) != -1)
        {
            switch (c)
            {
            case 'h':
                fprintf(stderr, "Usage: %s %s [<options>...]\n\n", prg, cmd);
                fprintf(stderr, "Poll for CAN bus for error classes reported by SocketCAN API.\n");
                fprintf(stderr, "Use CTRL-C to terminate.\n\n");
                fprintf(stderr, "Options:\n");
                fprintf(stderr, "   -p, --print   Print also non-error frames.\n\n");
                exit(0);
            case 'p':
                print_normal_frames = 1;
                break;
            case '?':
                fprintf(stderr, "Unknown option '-%c'. See '%s %s --help'.\n", optopt, prg, cmd);
                exit(1);
            default:
                abort();
            }
        }

        if (argc > 3)
        {
            fprintf(stderr, "Too many arguments. See '%s %s --help'.\n", prg, cmd);
            exit(1);
        }

        /* run error monitor */
        err = poll_errors(cf.ifname, print_normal_frames);
    }
    else if (strcmp("list", cmd) == 0)
    {
        unsigned char follow = 0;

        while ((c = getopt_long(argc, argv, "hf", base_options, &option_index)) != -1)
        {
            switch (c)
            {
            case 'h':
                fprintf(stderr, "Usage: %s %s [<options>...]\n\n", prg, cmd);
                fprintf(stderr, "List devices found in CAN bus.\n\n");
                fprintf(stderr, "Options:\n");
                fprintf(stderr, "   -f, --follow   Keep polling until CTRL-C. Refresh terminal in top style.\n\n");
                exit(0);
            case 'f':
                follow = 1;
                break;
            case '?':
                fprintf(stderr, "Unknown option '-%c'. See '%s %s --help'.\n", optopt, prg, cmd);
                exit(1);
            default:
                abort();
            }
        }

        if (argc > 3)
        {
            fprintf(stderr, "Too many arguments. See '%s %s --help'.\n", prg, cmd);
            exit(1);
        }

        /* list or poll status of found devices */
        err = list_devices(cf.ifname, follow);
    }
    else if (strcmp("show", cmd) == 0)
    {
        char *canid = NULL;

        while ((c = getopt_long(argc, argv, "hai:", base_options, &option_index)) != -1)
        {
            switch (c)
            {
            case 'h':
                fprintf(stderr, "Usage: %s %s [<args>...]\n\n", prg, cmd);
                fprintf(stderr, "Show parameters for given device or for all devices.\n\n");
                fprintf(stderr, "Arguments:\n");
                fprintf(stderr, "   -i, --id=<device id>   Device CAN id.\n");
                fprintf(stderr, "   -a, --all              Show all devices.\n\n");
                exit(0);
            case 'a':
                break;
            case 'i':
                canid = optarg;
                break;
            case '?':
                exit(1);
            default:
                abort();
            }
        }

        if (argc != 3)
        {
            fprintf(stderr, "Incorrect syntax. See '%s %s --help'.\n", prg, cmd);
            exit(1);
        }   

        if (canid)
        {
            if (strlen(canid) == 0)
            {
                fprintf(stderr, "Incorrect syntax. See '%s %s --help'.\n", prg, cmd);
                exit(1);
            }
        }

        /* show settings from all devices or from specificed device */
        err = show_devices(cf.ifname, canid);
    }
    else if (strcmp("get", cmd) == 0)
    {
        char *filename = NULL;

        while ((c = getopt_long(argc, argv, "f:h", setget_options, &option_index)) != -1)
        {
            switch (c)
            {
            case 'h':
                fprintf(stderr, "Usage: %s %s [<args>...]\n\n", prg, cmd);
                fprintf(stderr, "Save Oceanvolt device(s) parameters from CAN bus to configuration file.\n\n");
                fprintf(stderr, "Arguments:\n");
                fprintf(stderr, "   -f, --file=<filename>   Configuration file to write. Overwritten if exists.\n\n");
                fprintf(stderr, "E.g. \"ovutil get --file=ac50\"\n\n");
                exit(0);
            case 'f':
                filename = optarg;
                break;
            case '?':
                exit(1);
            default:
                abort();
            }
        }

        if ((argc != 3) || (filename == NULL))
        {
            fprintf(stderr, "Incorrect syntax. See '%s %s --help'.\n", prg, cmd);
            exit(1);
        }

        if (strlen(filename) == 0)
        {
            fprintf(stderr, "Incorrect syntax. See '%s %s --help'.\n", prg, cmd);
            exit(1);
        }

        /* create configuration from CAN bus and save it to file */
        err = save_config(cf.ifname, filename);
    }
    else if (strcmp("set", cmd) == 0)
    {
        char *filename = NULL;

        while ((c = getopt_long(argc, argv, "f:h", setget_options, &option_index)) != -1)
        {
            switch (c)
            {
            case 'h':
                fprintf(stderr, "Usage: %s %s [<args>...]\n\n", prg, cmd);
                fprintf(stderr, "Program Oceanvolt device(s) parameters from configuration file.\n\n");
                fprintf(stderr, "Arguments:\n");
                fprintf(stderr, "   -f, --file=<filename>   Configuration file to read.\n\n");
                fprintf(stderr, "E.g. \"ovutil set --file=imoca60\"\n\n");
                exit(0);
            case 'f':
                filename = optarg;
                break;
            case '?':
                exit(1);
            default:
                abort();
            }
        }

        if ((argc != 3) || (filename == NULL))
        {
            fprintf(stderr, "Incorrect syntax. See '%s %s --help'.\n", prg, cmd);
            exit(1);
        }

        if (strlen(filename) == 0)
        {
            fprintf(stderr, "Incorrect syntax. See '%s %s --help'.\n", prg, cmd);
            exit(1);
        }

        /* load configuration from file and write it to CAN bus */
        err = write_config(cf.ifname, filename);
    }
    else if (strcmp("setcanid", cmd) == 0)
    {
        char *oldid = NULL, *newid = NULL;

        while ((c = getopt_long(argc, argv, "o:n:h", setget_options, &option_index)) != -1)
        {
            switch (c)
            {
            case 'h':
                fprintf(stderr, "Usage: %s %s [<args>...]\n\n", prg, cmd);
                fprintf(stderr, "Change Oceanvolt device's CAN id.\n\n");
                fprintf(stderr, "Arguments:\n");
                fprintf(stderr, "   -o, --old=<device id>   Current CAN id.\n");
                fprintf(stderr, "   -n, --new=<device id>   New CAN id.\n\n");
                fprintf(stderr, "E.g. \"ovutil setcanid --old=0x3D0  --new=0x3E0\"\n\n");
                exit(0);
            case 'o':
                oldid = optarg;
                break;
            case 'n':
                newid = optarg;
                break;
            case '?':
                /* no error msg needed since getopt_long prints it */
                exit(1);
            default:
                abort();
            }
        }

        /* check that all arguments are set */
        if ((argc = !4) || (newid == NULL) || (oldid == NULL))
        {
            fprintf(stderr, "Incorrect syntax. See '%s %s --help'.\n", prg, cmd);
            exit(1);
        }

        /* prevent 0-length strings that getopt_long may parse if '--opt=' format is used */
        if ((strlen(newid) == 0) || (strlen(oldid) == 0))
        {
            fprintf(stderr, "Incorrect syntax. See '%s %s --help'.\n", prg, cmd);
            exit(1);
        }

        /* set new CAN ID for given device */
        err = set_canid(cf.ifname, oldid, newid);
    }
    else if (strcmp("resetcal", cmd) == 0)
    {
        char *canid = NULL;

        while ((c = getopt_long(argc, argv, "hi:", base_options, &option_index)) != -1)
        {
            switch (c)
            {
            case 'h':
                fprintf(stderr, "Usage: %s %s [<args>...]\n\n", prg, cmd);
                fprintf(stderr, "Reset calibration of given throttle device.\n\n");
                fprintf(stderr, "Arguments:\n");
                fprintf(stderr, "   -i, --id=<device id>   Throttle device CAN ID.\n");
                exit(0);
            case 'i':
                canid = optarg;
                break;
            case '?':
                exit(1);
            default:
                abort();
            }
        }

        if ((argc != 3) || (canid == NULL))
        {
            fprintf(stderr, "Incorrect syntax. See '%s %s --help'.\n", prg, cmd);
            exit(1);
        }

        if (strlen(canid) == 0)
        {
            fprintf(stderr, "Incorrect syntax. See '%s %s --help'.\n", prg, cmd);
            exit(1);
        }

        /* show settings from all devices or from specificed device */
        err = reset_throttle_calibration(cf.ifname, canid);
    }
    else
    {
        while ((c = getopt_long(argc, argv, "hv", base_options, &option_index)) != -1)
        {
            switch (c)
            {
            case 'v':
                print_version();
                exit(0);
            case 'h':
                print_usage();
                exit(0);
            case '?':
                fprintf(stderr, "Unknown option '-%c'. See '%s --help'.\n", optopt, prg);
                exit(1);
            default:
                abort();
            }
        }

        /* check for unknown arguments */
        fprintf(stderr, "Unknown arguments:");
        while (optind < argc)
        {
            fprintf(stderr, " '%s'", argv[optind++]);
        }

        fprintf(stderr, ". See '%s --help'.\n", prg);
        exit(1);
    }

    return err;
}