#include "sevcon.h"

namespace ovutil
{

Sevcon::Sevcon(canid_t id) : Controller(id, SEVCON, "Sevcon motor controller")
{
}

Sevcon::~Sevcon()
{
}

void Sevcon::update(const struct can_frame &frame)
{
    if (frame.can_id == getId())
    {
        poke();
    }
}

} // namespace ovutil