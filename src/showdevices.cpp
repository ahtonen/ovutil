#include <linux/can.h>
#include <cstring>
#include <iostream>
#include <memory>

#include "showdevices.h"
#include "assembly.h"
#include "util.h"
#include "lib.h"

int show_devices(const char *ifname, const char *canid_str)
{
    using namespace ovutil;

    int s;
    canid_t canid;
    std::unique_ptr<oceanvolt::Assembly> boat;

    if ((s = create_canbus_socket(ifname)) < 0)
    {
        return 1;
    }

    // Create boat's device assembly from CAN bus data stream
    try
    {
        // Use specific CAN device id hex string
        if (canid_str != nullptr)
        {
            hexstr2canid(canid_str, &canid);

            if (oceanvolt::Device::isValidCANID(canid) == false)
            {
                return 1;
            }
        }

        boat = oceanvolt::AssemblyFactory::createFromSocket(s);
    }
    catch (std::exception &e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }

    // Print only specific device or all
    if (canid_str != nullptr)
    {
        boat->printSettings(canid);
    }
    else
    {
        boat->printSettings();
    }

    if (close_canbus_socket(s) < 0)
    {
        return 1;
    }

    return 0;
}