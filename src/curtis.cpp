#include "curtis.h"

namespace ovutil
{

Curtis::Curtis(canid_t id) : Controller(id, CURTIS, "Curtis motor controller")
{
}

Curtis::~Curtis()
{
}

void Curtis::update(const struct can_frame &frame)
{
    if (frame.can_id == getId())
    {
        poke();
    }
}

} // namespace ovutil