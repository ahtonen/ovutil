#include <iostream>
#include "device.h"
#include "terminal.h"
#include "util.h"

namespace ovutil
{

Device::Device(canid_t id, const std::string &name, double timeout) : id(id),
                                                                      name(name),
                                                                      timeout(timeout),
                                                                      lastUpdateTime(std::chrono::system_clock::now())
{
}

canid_t Device::getId() const
{
    return id;
}

std::string Device::getIdString() const
{
    return canid2hexstr(id);
}

std::string Device::getName() const
{
    return name;
}

bool Device::isOnline() const
{
    std::chrono::duration<double> diff = std::chrono::system_clock::now() - lastUpdateTime;

    return (diff.count() < timeout);
}

void Device::poke()
{
    lastUpdateTime = std::chrono::system_clock::now();
}

namespace oceanvolt
{
Device::Device(canid_t id, typecode t, const std::string &name, double timeout) : ovutil::Device(id, name, timeout),
                                                                                  type(t),
                                                                                  softwareVersion("n/a")
{
}

void Device::parseSoftwareVersion(uint8_t major, uint8_t minor)
{
    softwareVersion = string_format("SW v%i.%i", major, minor);
}

void Device::printStatus() const
{
    if (isOnline())
    {
        std::cout << FGGREEN << "[  OK   ] " << FGWHITE;
    }
    else
    {
        std::cout << FGRED << "[TIMEOUT] " << FGWHITE;
    }
    // Arguments need to be C style strings since underlying implementation uses snprintf
    std::cout << string_format("0x%04X, %s, %s", id, name.c_str(), softwareVersion.c_str()) << std::endl;
}

bool Device::isDisplay(const struct can_frame &frame)
{
    // Skip EFF, RTR and error frames
    if (frame.can_id & ~CAN_ERR_MASK)
        return false;
    else if (frame.can_dlc != 4)
        return false;
    else if (frame.can_id & 0xF)
        return false;
    else if (frame.data[0] != OCEANVOLT_DISPLAY)
        return false;
    else
        return true;
}

bool Device::isDisplay(const std::string &str)
{
    return (str == typeToString(OCEANVOLT_DISPLAY));
}

bool Device::isThrottle(const struct can_frame &frame)
{
    // Skip EFF, RTR and error frames
    if (frame.can_id & ~CAN_ERR_MASK)
        return false;
    else if (frame.can_dlc != 5)
        return false;
    else if (frame.can_id & 0xF)
        return false;
    else if (frame.data[0] != OCEANVOLT_THROTTLE)
        return false;
    else
        return true;
}

bool Device::isThrottle(const std::string &str)
{
    return (str == typeToString(OCEANVOLT_THROTTLE));
}

bool Device::isValidCANID(canid_t canid)
{
    if (canid & ~CAN_SFF_MASK)
    {
        std::cerr << string_format("Value out of SFF address range. Should be within 0x0-0x%04X\n", CAN_SFF_MASK);
        return false;
    }
    else if (canid & 0x0000000FU)
    {
        std::cerr << "Invalid CAN ID. Lowest nibble must be always 0x0 for Oceanvolt devices." << std::endl;
        return false;
    }
    else
    {
        return true;
    }
}

std::string
Device::typeToString(typecode t)
{
    if (t == OCEANVOLT_DISPLAY)
    {
        return "display";
    }
    else if (t == OCEANVOLT_THROTTLE)
    {
        return "throttle";
    }
    else
    {
        throw std::invalid_argument("Unknown Oceanvolt device type code.");
    }
}

Device::typecode
Device::stringToType(const std::string &str)
{
    if (str == "display")
    {
        return OCEANVOLT_DISPLAY;
    }
    else if (str == "throttle")
    {
        return OCEANVOLT_THROTTLE;
    }
    else
    {
        throw std::invalid_argument("Unknown Oceanvolt device type string.");
    }
}

struct can_frame
Device::getCANIDChangeFrame(canid_t oldId, canid_t newId)
{
    struct can_frame frame;
    
    if(isValidCANID(newId) == false)
    {
        throw std::invalid_argument("Invalid new CAN ID");
    }

    frame.can_id = oldId + 4;
    frame.can_dlc = 5;
    packCANID(newId, frame.data);
    frame.data[4] = crc8(frame.data, frame.can_dlc - 1);

    return frame;
}

} // namespace oceanvolt

} // namespace ovutil