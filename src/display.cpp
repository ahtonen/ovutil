#include <iostream>
#include <sstream>
#include <string.h>
#include "display.h"
#include "terminal.h"
#include "util.h"

namespace ovutil
{
namespace oceanvolt
{

// Use 2.0s timeout
Display::Display(canid_t id,
                 bool regenerationEnabled,
                 uint8_t batteryLevelAtRegenerationStop,
                 uint16_t r1,
                 uint16_t r2,
                 uint16_t r3,
                 uint16_t r4,
                 uint16_t r5,
                 uint16_t r6,
                 uint32_t r7,
                 std::unique_ptr<Controller> c,
                 std::unique_ptr<Battery> b) : Device(id, OCEANVOLT_DISPLAY, "Oceanvolt Display", 2.0),
                                               status("n/a"),
                                               calibrationStatus("n/a"),
                                               regenerationEnabled(regenerationEnabled),
                                               batteryLevelAtRegenerationStop(batteryLevelAtRegenerationStop),
                                               u16regenerationParam{r1, r2, r3, r4, r5, r6},
                                               u32regenerationParam(r7),
                                               motorController(std::move(c)),
                                               battery(std::move(b))
{
}

Display::Display(canid_t id) : Display(id, false, 0, 0, 0, 0, 0, 0, 0, 0,
                                       nullptr, nullptr)
{
}

Display::~Display()
{
}

void Display::update(const struct can_frame &frame)
{
    if (frame.can_id == id)
    {
        poke();
        parseSoftwareVersion(frame.data[1], frame.data[2]);
        // status code at frame.data[3] not used currently
    }
    else if (frame.can_id == (id + 1))
    {
        poke();
        if (!battery)
        {
            battery = BatteryFactory::create(frame.data[4],
                                             unpackCANID(frame.data),
                                             unpackuint16(&frame.data[5]));
        }
    }
    else if (frame.can_id == (id + 2))
    {
        poke();
        if (!motorController)
        {
            motorController = ControllerFactory::create(frame.data[4],
                                                        unpackCANID(frame.data));
        }
        // Last bit indicates regeneration status. Display specific since not sent by Throttle.
        regenerationEnabled = (frame.data[5] & 0x01);
    }
    else if (frame.can_id == (id + 3))
    {
        poke();
        // Regeneration termination battery level threshold (% of max charge)
        batteryLevelAtRegenerationStop = frame.data[0];
        // Regeneration parameters 1-3
        u16regenerationParam[0] = unpackuint16(&frame.data[1]);
        u16regenerationParam[1] = unpackuint16(&frame.data[3]);
        u16regenerationParam[2] = unpackuint16(&frame.data[5]);
    }
    else if (frame.can_id == (id + 8))
    {
        poke();
        // Reply frame has full 8 byte payload with unused bytes filled with 0xFF
        if (frame.can_dlc == 8)
        {
            if (frame.data[0] == 0)
            {
                // Register 0: regeneration parameters 4-6
                u16regenerationParam[3] = unpackuint16(&frame.data[1]);
                u16regenerationParam[4] = unpackuint16(&frame.data[3]);
                u16regenerationParam[5] = unpackuint16(&frame.data[5]);
            }
            else if (frame.data[0] == 1)
            {
                // Register 1: regeneration parameter 7
                u32regenerationParam = unpackuint32(&frame.data[1]);
            }
        }
    }
    else if (motorController)
    {
        motorController->update(frame);
    }
    else if (battery)
    {
        battery->update(frame);
    }
}

void Display::printSettings() const
{
    using namespace std;
    string blank = "          ";

    // Device status line
    printStatus();

    // Battery
    if (battery)
    {
        cout << blank << string_format("%s configured at 0x%08X, capacity %iAh", battery->getName().c_str(), battery->getId(), battery->getCapacity()) << endl;
    }
    else
    {
        cout << blank << "Battery status message not received." << endl;
    }

    // Motor controller
    if (motorController)
    {
        cout << blank << string_format("%s configured at 0x%08X", motorController->getName().c_str(), motorController->getId()) << std::endl;
    }
    else
    {
        cout << blank << "Motor controller status message not received." << endl;
    }
    cout << blank << "Regeneration " << (regenerationEnabled ? "enabled" : "disabled") << endl;
    cout << blank << "Parameters:" << endl;
    cout << blank << string_format("  termination battery level (%% of max) = %3i", batteryLevelAtRegenerationStop) << endl;
    cout << blank << string_format("  1 = %5i", u16regenerationParam[0]) << endl;
    cout << blank << string_format("  2 = %5i", u16regenerationParam[1]) << endl;
    cout << blank << string_format("  3 = %5i", u16regenerationParam[2]) << endl;
    cout << blank << string_format("  4 = %5i", u16regenerationParam[3]) << endl;
    cout << blank << string_format("  5 = %5i", u16regenerationParam[4]) << endl;
    cout << blank << string_format("  6 = %5i", u16regenerationParam[5]) << endl;
    cout << blank << string_format("  7 = %5i", u32regenerationParam) << endl;
}

tinyxml2::XMLElement *
Display::getXMLElement(tinyxml2::XMLDocument &doc) const
{
    using namespace tinyxml2;

    XMLElement *device = doc.NewElement("Device");
    device->SetAttribute("id", getIdString().c_str());
    device->SetAttribute("type", oceanvolt::Device::typeToString(type).c_str());

    // Save Controller parameters
    motorController->saveAsXML(device, doc);

    // Save Battery parameters
    battery->saveAsXML(device, doc);

    // Regeneration parameters
    XMLElement *regen = doc.NewElement("Regeneration");
    regen->SetAttribute("enabled", regenerationEnabled);

    XMLElement *batt = doc.NewElement("BatteryLevelAtStop");
    batt->SetText(batteryLevelAtRegenerationStop);
    regen->InsertEndChild(batt);

    XMLElement *param;
    param = doc.NewElement("Parameter1");
    param->SetText(u16regenerationParam[0]);
    regen->InsertEndChild(param);

    param = doc.NewElement("Parameter2");
    param->SetText(u16regenerationParam[1]);
    regen->InsertEndChild(param);

    param = doc.NewElement("Parameter3");
    param->SetText(u16regenerationParam[2]);
    regen->InsertEndChild(param);

    param = doc.NewElement("Parameter4");
    param->SetText(u16regenerationParam[3]);
    regen->InsertEndChild(param);

    param = doc.NewElement("Parameter5");
    param->SetText(u16regenerationParam[4]);
    regen->InsertEndChild(param);

    param = doc.NewElement("Parameter6");
    param->SetText(u16regenerationParam[5]);
    regen->InsertEndChild(param);

    param = doc.NewElement("Parameter7");
    param->SetText(u32regenerationParam);
    regen->InsertEndChild(param);

    device->InsertEndChild(regen);

    return device;
}

std::unique_ptr<std::vector<struct can_frame>>
Display::getConfigChangeFrames() const
{
    std::unique_ptr<std::vector<struct can_frame>> frames = std::make_unique<std::vector<struct can_frame>>();
    struct can_frame frame;

    // Motor controller config
    frame.can_id = id + 5;
    frame.can_dlc = 7;
    packCANID(motorController->getId(), frame.data);
    frame.data[4] = motorController->getType();
    frame.data[5] = regenerationEnabled;
    frame.data[6] = crc8(frame.data, frame.can_dlc - 1);
    frames->push_back(frame);

    // Battery config
    frame.can_id = id + 6;
    frame.can_dlc = 8;
    packCANID(battery->getId(), frame.data);
    frame.data[4] = battery->getType();
    packuint16(battery->getCapacity(), &frame.data[5]);
    frame.data[7] = crc8(frame.data, frame.can_dlc - 1);
    frames->push_back(frame);

    // Regeneration parameters 1-3
    frame.can_id = id + 7;
    frame.can_dlc = 8;
    frame.data[0] = batteryLevelAtRegenerationStop;
    packuint16(u16regenerationParam[0], &frame.data[1]);
    packuint16(u16regenerationParam[1], &frame.data[3]);
    packuint16(u16regenerationParam[2], &frame.data[5]);
    frame.data[7] = crc8(frame.data, frame.can_dlc - 1);
    frames->push_back(frame);

    // Register 0: regeneration parameters 4-6
    frame.can_id = id + 8;
    frame.can_dlc = 8;
    frame.data[0] = 0;
    packuint16(u16regenerationParam[3], &frame.data[1]);
    packuint16(u16regenerationParam[4], &frame.data[3]);
    packuint16(u16regenerationParam[5], &frame.data[5]);
    frame.data[7] = crc8(frame.data, frame.can_dlc - 1);
    frames->push_back(frame);

    // Register 1: regeneration parameter 7
    frame.can_id = id + 8;
    frame.can_dlc = 8;
    frame.data[0] = 1;
    packuint32(u32regenerationParam, &frame.data[1]);
    frame.data[5] = 0xFF;
    frame.data[6] = 0xFF;
    frame.data[7] = crc8(frame.data, frame.can_dlc - 1);
    frames->push_back(frame);

    return frames;
}

void Display::readRegister(struct can_frame &frame, u_int8_t no) const
{
    memset(&frame, 0, sizeof(struct can_frame));
    frame.can_id = id + 8;
    frame.can_dlc = 1;
    frame.data[0] = no;
}

std::shared_ptr<Display>
DisplayFactory::create(tinyxml2::XMLElement *dev)
{
    using namespace tinyxml2;

    XMLElement *controller, *battery, *regen;

    controller = dev->FirstChildElement("Controller");
    if (controller == nullptr)
    {
        throw std::runtime_error("'Controller' element not found.");
    }

    battery = dev->FirstChildElement("Battery");
    if (battery == nullptr)
    {
        throw std::runtime_error("'Battery' element not found.");
    }

    regen = dev->FirstChildElement("Regeneration");
    if (regen == nullptr)
    {
        throw std::runtime_error("'Regeneration' element not found.");
    }

    bool regenerationEnabled;
    XMLError err = regen->QueryBoolAttribute("enabled", &regenerationEnabled);
    if (err != XML_SUCCESS)
    {
        std::ostringstream ss;
        ss << "Failed to parse attribute 'enabled' string to boolean. ";
        ss << "Valid strings: true, false.";

        throw std::runtime_error(ss.str());
    }

    return std::make_shared<Display>(parseIdAttribute(dev),
                                     regenerationEnabled,
                                     parseUint8Value(regen, "BatteryLevelAtStop"),
                                     parseUint16Value(regen, "Parameter1"),
                                     parseUint16Value(regen, "Parameter2"),
                                     parseUint16Value(regen, "Parameter3"),
                                     parseUint16Value(regen, "Parameter4"),
                                     parseUint16Value(regen, "Parameter5"),
                                     parseUint16Value(regen, "Parameter6"),
                                     parseUint32Value(regen, "Parameter7"),
                                     ControllerFactory::create(controller),
                                     BatteryFactory::create(battery));
}

} // namespace oceanvolt

} // namespace ovutil