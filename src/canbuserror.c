#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <linux/can.h>
#include <linux/can/raw.h>
#include <linux/can/error.h>
#include <errno.h>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/uio.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>

#include "canbuserror.h"
#include "lib.h"

#define MAX_LEN_ERROR_DESC 100

extern int running;

struct receive_stats
{
    unsigned long total_frames;
    unsigned long error_frames;
    float error_rate;
};

/* Decode only main error classes from can_id. See kernel include file can/error.h */
static void decode_error_frame(char *desc, const struct can_frame *frame)
{
    if (frame->can_id & CAN_ERR_TX_TIMEOUT)
    {
        sprintf(desc, "TX timeout (by netdevice driver).");
    }
    else if (frame->can_id & CAN_ERR_LOSTARB)
    {
        sprintf(desc, "Lost arbitration.");
    }
    else if (frame->can_id & CAN_ERR_CRTL)
    {
        sprintf(desc, "CAN interface controller problem.");
    }
    else if (frame->can_id & CAN_ERR_PROT)
    {
        sprintf(desc, "Protocol violation.");
    }
    else if (frame->can_id & CAN_ERR_TRX)
    {
        sprintf(desc, "Tranceiver status.");
    }
    else if (frame->can_id & CAN_ERR_ACK)
    {
        sprintf(desc, "Received no ACT on transmission.");
    }
    else if (frame->can_id & CAN_ERR_BUSOFF)
    {
        sprintf(desc, "Bus off.");
    }
    else if (frame->can_id & CAN_ERR_BUSERROR)
    {
        sprintf(desc, "Bus error (may flood!).");
    }
    else if (frame->can_id & CAN_ERR_RESTARTED)
    {
        sprintf(desc, "Controller restarted.");
    }
}

/* Read given CAN interface and show errors occurring. */
int poll_errors(const char *ifname, int show_normal_frames)
{
    fd_set rdfs;
    int s;
    unsigned int nbytes;
    struct sockaddr_can addr;
    struct can_frame frame;
    struct ifreq ifr;
    sigset_t orig_mask;
    struct receive_stats stats;
    char str[MAX_LEN_ERROR_DESC + 1];

    /* Create empty signal mask */
    sigemptyset(&orig_mask);

    /* Open the CAN network interface */
    if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0)
    {
        perror("Error while opening socket");
        return 1;
    }

    /* Get the index of the network interface */
    strncpy(ifr.ifr_name, ifname, IF_NAMESIZE);
    if (ioctl(s, SIOCGIFINDEX, &ifr) < 0)
    {
        perror("SIOCGIFINDEX");
        return 1;
    }

    /* Bind the socket to the network interface */
    addr.can_family = AF_CAN;
    addr.can_ifindex = ifr.ifr_ifindex;
    if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        perror("Error in socket bind");
        return 1;
    }

#ifdef DEBUG
    printf("%s at index %d\n", ifname, ifr.ifr_ifindex);
#endif

    /* enable reception of all error classes in can/error.h */
    can_err_mask_t err_mask = 0x000001FFU;
    setsockopt(s, SOL_CAN_RAW, CAN_RAW_ERR_FILTER, &err_mask, sizeof(err_mask));

    stats.total_frames = 0;
    stats.error_frames = 0;
    stats.error_rate = 0;

    int res;
    struct timespec read_timeout;
    memset(&read_timeout, 0, sizeof(struct timespec));
    // Set 0.5s timeout
    read_timeout.tv_nsec = 500000000U;

    printf("Polling CAN bus (hit Ctrl-C to break)...\n");
    usleep(500000);

    while (running)
    {
        // Monitor CAN socket file descriptor
        FD_ZERO(&rdfs);
        FD_SET(s, &rdfs);

        // Wait until there's something to read. Restart loop if interrupted by signal.
        // E.g. here most likely: SIGLARM, SIGINT
        // NOTE SIGLARM is disabled all this time except during the pselect call, when
        // signal mask is temporarily replated with orig_mask.
        res = pselect(s + 1, &rdfs, NULL, NULL, &read_timeout, &orig_mask);

        // interrupted by signal
        if (res == -1 && errno == EINTR)
        {
#ifdef DEBUG
            puts("interrupted by signal");
#endif
            continue;
        }
        // other error
        else if (res == -1 && errno != EINTR)
        {
            perror("pselect");
            return 1;
        }
        else if (res == 0)
        {
#ifdef DEBUG
            puts("timeout expired");
#endif
            continue;
        }

        if (FD_ISSET(s, &rdfs))
        {
            nbytes = read(s, &frame, sizeof(struct can_frame));

            if (nbytes < 0)
            {
                perror("CAN RAW socket read error.");
                return 1;
            }

            if (nbytes < sizeof(struct can_frame))
            {
                fprintf(stderr, "Incomplete CAN frame error: %i bytes read.", nbytes);
                return 1;
            }

            if (frame.can_id & CAN_ERR_FLAG)
            {
                decode_error_frame(str, &frame);
                /* alternative way to print error frames */
                //snprintf_can_error_frame(str, MAX_LEN_ERROR_DESC, (const struct canfd_frame *)&frame, "\n");
                fprintf(stdout, "%s\n", str);
                stats.error_frames++;
            }
            else
            {
                if (show_normal_frames)
                {
                    /* assume CAN 2.0 frames */
                    fprint_canframe(stdout, (struct canfd_frame *)&frame, "\n", 0, 8);
                }
                stats.total_frames++;
            }
        }
    }
    close(s);

    if (stats.total_frames > 0)
    {
        stats.error_rate = stats.error_frames / stats.total_frames;
    }
    
    fprintf(stdout, "\nTotal frames received: %8li\n", stats.total_frames);
    fprintf(stdout, "Error frames received: %8li\n\n", stats.error_frames);
    fprintf(stdout, "Error rate: %2.1f%%\n\n", stats.error_rate*100);

    return 0;
}