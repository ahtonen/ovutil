#include <iostream>
#include <sstream>
#include <cstring>
#include <chrono>
#include <memory>

#include <unistd.h>
#include <signal.h>
#include "assembly.h"
#include "display.h"
#include "throttle.h"
#include "util.h"
#include "tinyxml2.h"

namespace ovutil
{
namespace oceanvolt
{

Assembly::Assembly()
{
    // Reserve space for e.g. two Displays and Throttles
    devices.reserve(4);
}

Assembly::~Assembly()
{
}

bool Assembly::has(canid_t id) const
{
    for (auto const &dev : devices)
    {
        if (dev->getId() == id)
        {
            return true;
        }
    }

    return false;
}

bool Assembly::empty() const
{
    return devices.empty();
}

void Assembly::add(std::shared_ptr<oceanvolt::Device> dev)
{
    devices.push_back(dev);
}

void Assembly::update(const struct can_frame &frame)
{
    for (auto const &dev : devices)
    {
        dev->update(frame);
    }
}

void Assembly::printStatus() const
{
    for (auto const &dev : devices)
    {
        dev->printStatus();
    }
}

void Assembly::printSettings() const
{
    if (devices.empty() == true)
    {
        std::cout << "No devices found." << std::endl;
    }
    else
    {
        for (auto const &dev : devices)
        {
            dev->printSettings();
        }
    }
}

void Assembly::printSettings(canid_t id) const
{
    bool found = false;

    for (auto const &dev : devices)
    {
        if (dev->getId() == id)
        {
            dev->printSettings();
            found = true;
            break;
        }
    }

    if (found == false)
    {
        std::cout << string_format("Device with CAN id 0x%04X not found.", id) << std::endl;
    }
}

void Assembly::save(const char *fn) const
{
    using namespace tinyxml2;

    XMLDocument doc;
    XMLNode *root = doc.NewElement("Assembly");
    doc.InsertFirstChild(root);

    std::shared_ptr<Throttle> throttle;
    std::shared_ptr<Display> display;

    for (auto const &dev : devices)
    {
        if ((throttle = std::dynamic_pointer_cast<Throttle>(dev)))
        {
            root->InsertFirstChild(throttle->getXMLElement(doc));
        }
        else if ((display = std::dynamic_pointer_cast<Display>(dev)))
        {
            root->InsertFirstChild(display->getXMLElement(doc));
        }
        else
        {
            throw std::runtime_error("oceanvolt::Device has neither Throttle or Display object in it");
        }
    }

    XMLError err = doc.SaveFile(fn);
    if (err != XML_SUCCESS)
    {
        throw std::runtime_error(doc.ErrorStr());
    }

    std::cout << "Settings saved to '" << fn << "'." << std::endl;
}

void Assembly::write(int socket) const
{
    std::unique_ptr<std::vector<struct can_frame>> frames;

    // Loop all Oceanvolt devices
    for (auto const &dev : devices)
    {
        frames = dev->getConfigChangeFrames();

        // Loop all configuration frames and write them to CAN bus
        for (std::vector<struct can_frame>::const_iterator it = frames->begin();
             it != frames->end();
             ++it)
        {
            write_canbus_socket(socket, *it);
        }
    }
}

std::unique_ptr<Assembly>
AssemblyFactory::createFromSocket(int socket)
{
    using namespace std::chrono;

    fd_set rdfs;
    sigset_t zero_mask;
    struct timespec read_timeout;
    int res;
    struct can_frame frame;

    system_clock::time_point start = system_clock::now();
    duration<double> diff;
    std::unique_ptr<Assembly> boat = std::make_unique<Assembly>();

    sigemptyset(&zero_mask);
    // Set 0.5s read timeout
    memset(&read_timeout, 0, sizeof(struct timespec));
    read_timeout.tv_nsec = 500000000U;

    std::cout << string_format("Polling CAN bus for %2.1f seconds...\n", AssemblyFactory::POLLING_TIME);

    while (1)
    {
        diff = system_clock::now() - start;
        if (diff.count() > AssemblyFactory::POLLING_TIME)
        {
            break;
        }

        // Monitor CAN socket file descriptor
        FD_ZERO(&rdfs);
        FD_SET(socket, &rdfs);

        // Wait until there's something to read. Restart loop if interrupted by signal.
        // E.g. here most likely: SIGLARM, SIGINT
        // NOTE SIGLARM is disabled all this time except during the pselect call, when
        // signal mask is temporarily replated with orig_mask.
        res = pselect(socket + 1, &rdfs, NULL, NULL, &read_timeout, &zero_mask);

        // interrupted by signal
        if (res == -1 && errno == EINTR)
        {
#ifdef DEBUG
            std::cout << "interrupted by signal" << std::endl;
#endif
            continue;
        }
        // other error
        else if (res == -1 && errno != EINTR)
        {
            std::perror("pselect");
            throw std::runtime_error("pselect");
        }
        else if (res == 0)
        {
#ifdef DEBUG
            std::cout << "timeout expired" << std::endl;
#endif
            continue;
        }

        if (FD_ISSET(socket, &rdfs))
        {
            read_canbus_socket(socket, &frame);

            // Add new devices
            if (oceanvolt::Device::isDisplay(frame) == true)
            {
                if (boat->has(frame.can_id) == false)
                {
                    std::shared_ptr<Display> d = std::make_shared<Display>(frame.can_id);
                    can_frame query;
                    // Query messages for parameters in registers 0 and 1
                    d->readRegister(query, 0);
                    write_canbus_socket(socket, query);
                    d->readRegister(query, 1);
                    write_canbus_socket(socket, query);

                    boat->add(d);
                }
            }
            if (oceanvolt::Device::isThrottle(frame) == true)
            {
                if (boat->has(frame.can_id) == false)
                {
                    boat->add(std::make_shared<Throttle>(frame.can_id));
                }
            }
            // Parse frame
            boat->update(frame);
        }
    }

    if (boat->empty())
    {
        std::cout << "No devices found." << std::endl;
    }

    // boat will surrender ownership of pointer
    return boat;
}

std::unique_ptr<Assembly> AssemblyFactory::createFromConfigFile(const char *fn)
{
    using namespace tinyxml2;

    std::unique_ptr<Assembly> boat = std::make_unique<Assembly>();
    XMLDocument doc;
    XMLError err;
    const char *str;

    err = doc.LoadFile(fn);
    if (err != XML_SUCCESS)
    {
        throw std::invalid_argument(doc.ErrorStr());
    }

    // Get root node
    XMLNode *root = doc.FirstChild();
    if (root == nullptr)
    {
        throw std::runtime_error("File does not contain valid XML.");
    }

    // Iterate all Devices
    XMLElement *dev = root->FirstChildElement("Device");
    while (dev != nullptr)
    {
        // Get device type
        str = dev->Attribute("type");
        if (str == nullptr)
        {
            throw std::runtime_error("Attribute 'type' not found under 'Device'.");
        }
        std::string type(str);

        // Create devices
        std::exception_ptr p;
        try
        {
            if (oceanvolt::Device::isThrottle(type))
            {
                boat->add(oceanvolt::ThrottleFactory::create(dev));
            }
            else if (oceanvolt::Device::isDisplay(type))
            {
                boat->add(oceanvolt::DisplayFactory::create(dev));
            }
            else
            {
                std::ostringstream ss;
                ss << "Invalid device 'type' attribute: " << type << ". Valid types: ";
                ss << Device::typeToString(Device::OCEANVOLT_DISPLAY) << ", ";
                ss << Device::typeToString(Device::OCEANVOLT_THROTTLE) << ".";

                throw std::runtime_error(ss.str());
            }
        }
        catch (std::exception &e)
        {
            p = std::current_exception();

            // Print TinyXML error message if applicable
            if (doc.Error())
            {
                std::cerr << doc.ErrorStr() << std::endl;
            }

            std::rethrow_exception(p);
        }

        // Next device
        dev = dev->NextSiblingElement("Device");
    }

    return boat;
}

} // namespace oceanvolt

} // namespace ovutil