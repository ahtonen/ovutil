#include "piktronik.h"

namespace ovutil
{

Piktronik::Piktronik(canid_t id) : Controller(id, PIKTRONIK, "Piktronik motor controller")
{
}

Piktronik::~Piktronik()
{
}

void Piktronik::update(const struct can_frame &frame)
{
    if (frame.can_id == getId())
    {
        poke();
    }
}

} // namespace ovutil