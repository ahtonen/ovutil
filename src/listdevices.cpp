#include <linux/can.h>
#include <linux/can/raw.h>
//#include <sys/types.h>
#include <unistd.h>
#include <signal.h>

#include <time.h>
#include <cstring>
#include <iostream>
#include <memory>

#include "listdevices.h"
#include "assembly.h"
#include "display.h"
#include "throttle.h"
#include "terminal.h"
#include "util.h"

#define MAX_POLLING_TIME_SEC 5

extern sig_atomic_t running;
static ovutil::oceanvolt::Assembly boat;

static void printstats(int signo)
{
    printf("%s", CLR_SCREEN);
    printf("%s", CSR_HOME);

    time_t currtime;
    struct tm now;

    if (time(&currtime) == (time_t)-1)
    {
        std::perror("time");
        exit(1);
    }

    localtime_r(&currtime, &now);

    printf("%04d-%02d-%02d %02d:%02d:%02d ",
           now.tm_year + 1900,
           now.tm_mon + 1,
           now.tm_mday,
           now.tm_hour,
           now.tm_min,
           now.tm_sec);

    printf("\n");

    boat.printStatus();
    fflush(stdout);
    alarm(1);
}

int list_devices(const char *ifname, unsigned char follow)
{
    using namespace ovutil;

    fd_set rdfs;
    int s, res;
    unsigned int nbytes;
    struct can_frame frame;
    struct sigaction sa;
    sigset_t orig_mask, zero_mask;
    sigset_t block_alarm_mask;
    time_t start, now;
    struct timespec read_timeout;

    // Signal mask for blocking alarm
    sigemptyset(&block_alarm_mask);
    if (sigaddset(&block_alarm_mask, SIGALRM) == -1)
    {
        std::perror("sigaddset");
        return 1;
    }
    sigemptyset(&zero_mask);

    // Create signal handler for printing to console
    sa.sa_handler = printstats;
    sa.sa_flags = 0;
    sigemptyset(&sa.sa_mask);

    // Block Ctrl-C (SIGINT) during execution of handler
    if (sigaddset(&sa.sa_mask, SIGINT) == -1)
    {
        std::perror("sigaddset");
        return 1;
    }

    // Assign handler
    if (sigaction(SIGALRM, &sa, NULL) == -1)
    {
        std::perror("sigaction");
        return 1;
    }

    // Open socket
    if ((s = create_canbus_socket(ifname)) == -1)
    {
        return 1;
    }

    if (follow == 1)
    {
        // Disable alarm for now
        sigprocmask(SIG_BLOCK, &block_alarm_mask, &orig_mask);
        // Send SIGLARM after 1s
        alarm(1);
        printf("Polling CAN bus (hit Ctrl-C to break)...\n");
        usleep(500000);
        printf("%s", CLR_SCREEN);
    }
    else
    {
        printf("Polling CAN bus for %i seconds...\n", MAX_POLLING_TIME_SEC);
    }

    // Set 0.5s read timeout
    memset(&read_timeout, 0, sizeof(struct timespec));
    read_timeout.tv_nsec = 500000000U;

    time(&start);
    while (running)
    {
        if (follow == 0)
        {
            time(&now);
            if (difftime(now, start) > MAX_POLLING_TIME_SEC)
            {
                running = 0;
            }
        }

        // Monitor CAN socket file descriptor
        FD_ZERO(&rdfs);
        FD_SET(s, &rdfs);

        // Wait until there's something to read. Restart loop if interrupted by signal.
        // E.g. here most likely: SIGLARM, SIGINT
        // NOTE SIGLARM is disabled all this time except during the pselect call, when
        // signal mask is temporarily replated with orig_mask.
        res = pselect(s + 1, &rdfs, NULL, NULL, &read_timeout, &orig_mask);

        // interrupted by signal
        if (res == -1 && errno == EINTR)
        {
#ifdef DEBUG
            puts("interrupted by signal");
#endif
            continue;
        }
        // other error
        else if (res == -1 && errno != EINTR)
        {
            std::perror("pselect");
            return 1;
        }
        else if (res == 0)
        {
#ifdef DEBUG
            puts("timeout expired");
#endif
            continue;
        }

        if (FD_ISSET(s, &rdfs))
        {
            using namespace ovutil::oceanvolt;

            nbytes = ::read(s, &frame, sizeof(struct can_frame));

            if (nbytes < 0)
            {
                std::perror("can raw socket read");
                return 1;
            }

            if (nbytes < sizeof(struct can_frame))
            {
                std::cerr << "read: incomplete CAN frame" << std::endl;
                return 1;
            }

            // Add new devices
            if (oceanvolt::Device::isDisplay(frame) == true)
            {
                if (boat.has(frame.can_id) == false)
                {
                    can_frame query;
                    std::shared_ptr<oceanvolt::Display> d = std::make_shared<oceanvolt::Display>(frame.can_id);

                    // Write messages for parameters that need to be queried
                    // TBD more bus checks needed, e.g. in case bus does down or write registers fill
                    d->readRegister(query, 0);
                    
                    nbytes = ::write(s, &query, sizeof(struct can_frame));
                    if (nbytes < 0)
                    {
                        std::perror("can raw socket write");
                        return 1;
                    }
                    else if (nbytes < sizeof(struct can_frame))
                    {
                        std::cerr << "write: incomplete CAN frame" << std::endl;
                        return 1;
                    }

                    d->readRegister(query, 1);
                    nbytes = ::write(s, &query, sizeof(struct can_frame));
                    if (nbytes < 0)
                    {
                        std::perror("can raw socket write");
                        return 1;
                    }
                    else if (nbytes < sizeof(struct can_frame))
                    {
                        std::cerr << "write: incomplete CAN frame" << std::endl;
                        return 1;
                    }

                    boat.add(d);
                }
            }
            if (oceanvolt::Device::isThrottle(frame) == true)
            {
                if (boat.has(frame.can_id) == false)
                {
                    boat.add(std::make_shared<oceanvolt::Throttle>(frame.can_id));
                }
            }
            // Parse frame. Block alarm during parsing.
            boat.update(frame);
        }
    }

    if (follow == 0)
    {
        if (boat.empty())
        {
            printf("No devices found.\n");
        }
        else
        {
            boat.printStatus();
        }
    }

    close(s);
    return 0;
}
