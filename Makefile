CXX = g++
CPPFLAGS=-std=c++14 -I$(INCLUDE)
CXXFLAGS = -Wall
# Use C++ compiler for C sources as well
CC = $(CXX)
CFLAGS = $(CXXFLAGS)
LDFLAGS =
LDLIBS =
RM = rm -f
CP = cp -a

BIN = bin
SRC = src
INCLUDE = include
INSTALL = /usr/local
TARGET = ovutil

.PHONY: default all run clean install uninstall depend

sources = $(wildcard $(SRC)/*.c) $(wildcard $(SRC)/*.cpp)
objects = $(patsubst %.c, %.o, $(patsubst %.cpp, %.o, $(sources)))

# Make release build by default
default: release
all: default

debug: CXXFLAGS += -DDEBUG -g
debug: $(BIN)/$(TARGET)

release: CXXFLAGS += -O2
release: $(BIN)/$(TARGET)

depend: .depend

test:
	@echo $(objects)

# Run preprocessor to create dependencies (obsolete in newer make?)
.depend: $(sources)
	$(RM) ./.depend
	$(CXX) $(CPPFLAGS) -M $^>>./.depend;


install: $(BIN)/$(TARGET)
	mkdir -p $(INSTALL)/bin
	$(CP) $< $(INSTALL)/bin/

uninstall:
	$(RM) $(INSTALL)/bin/$(TARGET)

clean:
	$(RM) $(objects)
	$(RM) ./.depend
	$(RM) $(BIN)/$(TARGET)

# Link target. Implicit rules are used to build object files.
$(BIN)/$(TARGET): $(objects)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o $@ $(objects) $(LDLIBS)
	
